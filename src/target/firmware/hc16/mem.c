/* Optimized paged memory access functions for HC16
 * Must be built with -O0
 * 
 * This file is part of osmo-tetra-bb.
 *
 * Copyright (C) 2015-2021 by Steve Markgraf <steve@steve-m.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 * osmo-tetra-bb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * osmo-tetra-bb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm.h>
#include <hc16/mem.h>

uint16_t readw_p(uint32_t addl)
{
	uint16_t val;

	asm("pshm    b,e,x,k\n\t"
	    "ldab    %1\n\t"			/* load page to B */
	    "tbxk\n\t"				/* transfer B to XK */
	    "ldx     %2\n\t"			/* load address to X */
	    "lde     0,x\n\t"			/* read memory to E with offset 0 + X*/
	    "ste     %0\n\t"			/* store result */
	    "pulm    b,e,x,k\n\t" 
	    : "=memory" (val)			/* output operand, %0 */
	    : "memory" (PAGEBYTE(addl)), 	/* input operands, %1 */
	      "memory" (MEMWORD(addl)));	/* input operands, %2 */

	return val;
}

void writew_p(uint16_t val, uint32_t addl)
{
	asm("pshm    b,k\n\t"
	    "ldab    %0\n\t"		/* load page to B */
	    "tbxk\n\t"			/* transfer B to XK */
	    "ldx     %1\n\t"
	    "lde     %2\n\t"
	    "ste     0,x\n\t"
	    "pulm    b,k\n\t" 
	    :					/* no output operands */
	    : "memory" (PAGEBYTE(addl)), 	/* input operands, %0 */
	      "memory" (MEMWORD(addl)),		/* input operands, %1 */
	      "memory" (val)			/* input operands, %2 */
	    : "e", "x");		// fixme, add bk and k to clobber list!
}

uint8_t readb_p(uint32_t addl)
{
	uint8_t val;

	asm("pshm    b,k\n\t"
	    "ldab    %1\n\t"			/* load page to B */
	    "tbxk\n\t"				/* transfer B to XK */
	    "ldx     %2\n\t"			/* load address to X */
	    "ldab    0,x\n\t"			/* read memory to B with offset 0 + X*/
	    "stab    %0\n\t"			/* store result */
	    "pulm    b,k\n\t" 
	    : "=memory" (val)			/* output operand, %0 */
	    : "m" (PAGEBYTE(addl)), 		/* input operands, %1 */
	      "m" (MEMWORD(addl))		/* input operands, %2 */
	    : "x");				// fixme, add bk and k to clobber list!

	return val;
}

void writeb_p(uint8_t val, uint32_t addl)
{
	asm("pshm    b,k\n\t"
	    "ldab    %0\n\t"		/* load page to B */
	    "tbxk\n\t"			/* transfer B to XK */
	    "ldx     %1\n\t"
	    "ldab    %2\n\t"
	    "stab    0,x\n\t"
	    "pulm    b,k\n\t" 
	    :				/* no output operands */
	    : "m" (PAGEBYTE(addl)), 	/* input operand, %0 */
	      "m" (MEMWORD(addl)),	/* input operand, %1 */
	      "m" (val)			/* input operand, %2 */
	    : "x");		// fixme, add bk and k to clobber list!
}

/* memcopy read function that can handle different source pages,
 * maximum length 64K bytes */
void readmem(uint32_t src, void *dest, uint16_t len)
{
	asm("pshm    b,k\n\t"
	    "ldab    %0\n\t"		/* load src page to B */
	    "tbxk\n\t"			/* transfer src page to XK */
	    "ldab    %1\n\t"		/* load dest page to B */
	    "tbyk\n\t"			/* transfer dest page to YK */
	    "ldx     %2\n\t"		/* src addr in X */
	    "ldy     %3\n\t"		/* dest addr in Y */
	    "clre\n\t"
     "loop:  ldab    e,x\n\t"
	    "stab    e,y\n\t"
	    "adde    #1\n\t"
	    "cpe     %4\n\t"
	    "beq    done\n\t"
	    "bra     loop\n\t"
     "done:  pulm    b,k\n\t"
	    :					/* no output operands */
	    : "memory" (PAGEBYTE(src)),		/* input operand, %0 */
	      "X" (0xf),			/* input operand, %1, destination page */
	      "memory" (MEMWORD(src)),		/* input operand, %2 */
	      "memory" (dest),			/* input operand, %3 */
	      "memory" (len)			/* input operand, %4 */
	    : "x", "y", "e");		// fixme, add bk and k to clobber list!
}
