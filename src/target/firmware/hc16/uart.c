#include <asm.h>
#include <hc16/mem.h>
#include <lib/lib.h>
#include <interrupts.h>
#include <hc16/uart.h>
#include <hc16/regs.h>

#define RIE (1 << 5)		/* receive interrupt enable */

#define RX_BUF_SIZE	128
#define RX_BUF_MASK	(RX_BUF_SIZE - 1)

#define USE_INTERRUPTS

char rx_buf[RX_BUF_SIZE];
volatile uint16_t head = 0, tail = 0;

static inline void char_enqueue(char c)
{
	rx_buf[head] = c;
	head = (head + 1) & RX_BUF_MASK;

//	if (head == tail)
//		puts("RX overrun!\n");
}

static inline char char_dequeue(void)
{
	char c;

	/* black until data arrives */
	while (head == tail) { };
	c = rx_buf[tail];
	tail = (tail + 1) & RX_BUF_MASK;

	return c;
}

void uart_init(void)
{
//	disable_interrupts();

	/* "Each module that can generate interrupt requests must be assigned a
	 * unique, non-zero IARB field value in order to request an interrupt"
	 * If we don't to that, the configured interrupt vector number is ignored and the interrupt
	 * ends up in the autovector 7 instead */
	writew(0x0008, QSMCR);

	/** initialize SCI as UART **/
	/* set RXDA and TXDA to SCI mode */
	writew((1 << 7) | (1 << 6) , MDDR);

	/* Baud rate must be set before the SCI is enabled. */
	
	//FSYS = 16.8 MHz => p. 434   27 for 19200
	/* set baudrate divider (SCBR) */
	writew(4, SCCR0);

	/* use interrupt vector 0, priority 1 */
	writew(0x0140, QILR);

	/* UART config, standard 8N1, no interrupts, enable RX and TX */
	writew(SCCR_TE | SCCR_RE | RIE, SCCR1);
}

void putc(char c)
{
	uint16_t ret = 0;

	while (!(ret & SCSR_TDRE))
		ret = readw(SCSR);

	writew(c, SCDR);
}

void uart_putdata(uint8_t *data, int len)
{
	uint16_t ret;
	int i;

	for (i = 0; i < len; i++) {
		ret = 0;
		while (!(ret & SCSR_TDRE))
			ret = readw(SCSR);

		writew(data[i], SCDR);
	}
}

char getchar(void)
{
#ifdef USE_INTERRUPTS
	return char_dequeue();
#else
	uint16_t ret = 0;

	while (!(ret & SCSR_RDRF))
		ret = readw(SCSR);

	return readw(SCDR) & 0xff;
#endif
}

//char getchar_irq(void)
//{
//	return char_dequeue();
//}

void puts(const char *s)
{
	char c;
	while ((c = *s++) != '\0') {
		putc(c);
		if (c == '\n')
			putc('\r');
	}
}

#if 0
void uart_interrupt(void)
{
	uint16_t ret = 0;
	uint16_t data;

	ret = readw(SCSR);

	while (ret & SCSR_RDRF)
	{
		data = readw(SCDR);
		putc(data);
		char_enqueue(data & 0xff);
		ret = readw(SCSR);
	}
}
#endif
