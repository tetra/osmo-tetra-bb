/* SPI driver for the HC16 QSPI module
 *
 * This file is part of osmo-tetra-bb.
 *
 * Copyright (C) 2015-2022 by Steve Markgraf <steve@steve-m.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 * osmo-tetra-bb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * osmo-tetra-bb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm.h>
#include <hc16/regs.h>
#include <hc16/mem.h>
#include <hc16/spi.h>

/* PCS2: Analog ASIC, chipselect is active high */
#define CS_IDLE_MASK	0x3

void spi_send(uint16_t *val, uint8_t len, uint8_t cs)
{
	uint8_t cmd, ret;
	uint8_t cs_mask = CS_IDLE_MASK;
	uint16_t cmd_ram_ptr = SPI_CR(0);	/* doing it this way due to a compiler optimization bug */
	uint16_t end_transfer_on_byte = len & TRANSFER_END_ON_BYTE;
	uint8_t i;

	len &= ~TRANSFER_END_ON_BYTE;

	/* TODO: retrieve the mask using an array for cs 0/1/2 */
	if (cs == 2)
		cs_mask |= (1 << 2);
	else
		cs_mask &= (cs == 0) ? ~0x01 : ~0x02;

	for (i = 0; i < len; i++) {
		writew(val[i], (SPI_TR(0) + (i*2)));//SPI_TR(i)); 0 1 0//

		cmd = CR_BITSE | CR_DT | CR_DSCK | cs_mask;

		if (i < (len-1)) {
			cmd |= CR_CONT;
		} else {
			if (end_transfer_on_byte)
				cmd &= ~CR_BITSE;
		}

		writeb(cmd, cmd_ram_ptr++);
	}

	writew(((len - 1) << 8), SPCR2);

	/* enable QSPI */
	writew(SPCR_SPE | 0x303, SPCR1);

	while (!(ret & SPSR_SPIF))
		ret = readb(SPSR);
}

void spi_init(void)
{
	uint16_t ret = 0;
	uint16_t cmd_ram_ptr = SPI_CR(0);
	uint8_t i = 0;

	/* read and clear flags in SPSR */
	ret = readw(SPSR);
	ret = 0;
	writew(ret, SPSR);

	writew(0x3bfe, PQSPAR);

	/* select idle state of chip select lines: PCS0/1 high, PCS2 low */
	writew(CS_IDLE_MASK << 3, PORTQS);

	/* SPI settings: Master, CPOL0, CPHA0, */
	writew(SPCR_MSTR | 0x009, SPCR0);

	writeb(0, SPCR3);

	/* clear the QSPI command RAM */
	for (i = 0; i < 16; i++)
		writeb(0, cmd_ram_ptr++);
}
