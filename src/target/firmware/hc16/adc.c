#include <asm.h>
#include <hc16/regs.h>
#include <hc16/mem.h>
#include <hc16/adc.h>
#include <lib/lib.h>

/* The hardware ID on the Motorola MTH300/d700 is set through resistive voltage
 * dividers on the ADC channels 6 and 7. The original algorithm is a bit more
 * complex, we only care about differentiating hardware code Ac and Bc for now.
 */

#define HW_AC_ADC_VAL	0xd4
#define HW_BC_ADC_VAL	0xed
#define HW_VAL_MARGIN	((HW_BC_ADC_VAL - HW_AC_ADC_VAL) / 2)

uint8_t adc_read_hwcode(void)
{
	int i;
	uint16_t val = 0;
	uint8_t hwcode = 0;

	writew(0, ADCMCR);
	writew(0x44, ADCTL0);
	writew(0x70, ADCTL1);

	while (!(readw(ADCSTAT) & (1 << 15))) {};

	/* average over 16 samples */
	for (i = 0; i < 16; i++)
		val += readw(RJURR7);

	val >>= 4;

	if ((val < HW_AC_ADC_VAL + HW_VAL_MARGIN) && (val > HW_AC_ADC_VAL - HW_VAL_MARGIN))
		hwcode = 0xac;
	else if ((val < HW_BC_ADC_VAL + HW_VAL_MARGIN) && (val > HW_BC_ADC_VAL - HW_VAL_MARGIN))
		hwcode = 0xbc;
	else
		printf("Unknown HW code, ADC val: %04x\n", val);

	dprintf("ADC val %04x, hw code %04x\n", val, hwcode);

	return hwcode;
}
