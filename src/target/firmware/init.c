#include <init.h>
#include <asm.h>
#include <hc16/regs.h>
#include <hc16/mem.h>
#include <hc16/uart.h>
#include <hc16/spi.h>
#include <hc16/adc.h>
#include <interrupts.h>
#include <drivers/flash.h>
#include <drivers/keypad.h>
#include <drivers/pmic.h>
#include <drivers/display.h>
#include <lib/lib.h>

#define BACKLIGHT_ON 1

/* Memory Map:
 * 
 * 0x00000 - 0x007ff:	mapped internal SRAM used to store interrupt vectors when running from ext. SRAM
 * 0x00800 - 0x7ffff:	lower flash window (lower 512K of flash, static)
 * 0x80000 - 0xbffff:	upper flash window (upper 1024K mapped into a 256K window, page selected by PORTGP, see flash.c)
 * 0xc0000 - 0x
 * 
 * 
 * 
 * */

#if 1
static void init_cs(void)
{
	/* initialize chip selects, p. 378 DS */
	writeb(0xff, 0xffa41);		//figure out what that does (undocumented)

	/* configure the chip select pin assignments */
	writew(0x0ebf, CSPAR0);
	writew(0x0200, CSPAR1);

	/* map flash at 0x800 so we can map internal SRAM at 0x0 for interrupt vectors */
	writew(0x0086, CSBARBT);	/* 512 Kbytes @ 0x000800: Flash, originally begins at 0x0 */
	writew(0x7870, CSORBT);		//0x7c30/* Async mode, both bytes, rw, addr strobe, 0 WS */


//bdm_wdmem(   0xffA4C, 0xf805);
//bdm_wdmem(   0xffa4e, 0x7870);

#if 1
	writew(0xf805, CSBAR0);		/* 256 Kbytes @ 0xF80000: paged flash */
	writew(0x7c30, CSOR0);		/* Async mode, both bytes, rw, addr strobe, 0 WS */
#endif


	writew(0xfe03, CSBAR2);		/* 64 Kbytes @ 0xFE0000: Codeplug EEPROM */
	writew(0x5cb0, CSOR2);		/* Async mode, upper byte, rw, data strobe, 2 WS */

	writew(0xfc00, CSBAR3);		/* 2 Kbytes @ 0xFF0000: DSP */
	writew(0x5830, CSOR3);		/* Async mode, upper byte, rw, addr strobe, 0 WS */

	writew(0xff03, CSBAR1);		/* 128 Kbytes @ 0xFC0000: lower external SRAM */
	writew(0x3870, CSOR1);		/* Async mode, lower byte, rw, data strobe, 1 WS */
	writew(0xff03, CSBAR4);		/* 128 Kbytes @ 0xFC0000: upper external SRAM  */
	writew(0x5870, CSOR4);		/* Async mode, upper byte, rw, data strobe, 1 WS */

	writew(0xFFF9, CSBAR5);		/* Enable autovector generation for interrupt handling */
	writew(0x7801, CSOR5);		/* See example code in CPU16RM */

	writew(0x0000, CSBAR6);
	writew(0x0000, CSOR6);
	writew(0x0000, CSBAR7);
	writew(0x0000, CSOR7);
	writew(0x0000, CSBAR8);
	writew(0x0000, CSOR8);
	writew(0x0000, CSBAR9);
	writew(0x0000, CSOR9);

	writew(0xfe00, CSBAR10);	/* 2 Kbytes @ 0xFF1000: Display */
	writew(0x5930, CSOR10);		/* Async mode, upper byte, rw, addr strobe, 4 WS */
}
#endif



/* Initialize 4.8 MHz clock used for CPU and DSP on TCLKB output of 57W85 "ADDAG" ASIC 
 * "Analog-to-Digital, Digital-to-Analog and Glue" */
void clockchip_init(void)
{
	/* sniffed with OLS, settings:
	 * SPI mode 0, 16 bit, MSB first, honour CS
	*/
	uint16_t t1[] = { 0x1082, 0x2445, 0x0001 };
	uint16_t t2[] = { 0x1067, 0x2445, 0x0003 };
	//short pause
	uint16_t t3[] = { 0x1067, 0x2445, 0x0003 };
	//<here would be a transfer to CS1>
	uint16_t t4[] = { 0x1082, 0x2445, 0x0001 };


//	uint16_t t5[] = { 0x1067, 0x2445, 0x0003 };
	uint16_t t6[] = { 0x1082, 0x2445, 0x0001 };



	spi_send(t1, ARRAY_SIZE(t1), ADDAG_CS);
	delay_ms(3);
	spi_send(t2, ARRAY_SIZE(t2), ADDAG_CS);
	delay_ms(1);
	spi_send(t3, ARRAY_SIZE(t3), ADDAG_CS);
	delay_ms(2);
	spi_send(t4, ARRAY_SIZE(t4), ADDAG_CS);
	delay_ms(1);

	rf_init();
//	rf_query();

	spi_send(t3, ARRAY_SIZE(t3), ADDAG_CS);
	delay_ms(1);
	spi_send(t6, ARRAY_SIZE(t6), ADDAG_CS);



 //def
	delay_ms(1);
	// <big chunk to CS1>

	rf_query();



	delay_ms(1);


	spi_send(t3, ARRAY_SIZE(t3), ADDAG_CS);



	//uint16_t t7[] = {0x1067, 0x2445, 0x0003 };

	//reg 1 bit 12, reg 2 bit 13: enables 4MHz for CPU, 2MHz for DSP
	//uint16_t eight_mhz[] = {0x1000, 0x2000, 0x0600 };// <- generates 8M cpu, 4M DSP
	//uint16_t test[] = {0x1000, 0x2000, 0x0600 };
//	spi_send(t6, 3, 0);

}

void init(void)
{
	uint16_t val;
	uint8_t hw_code;

	/* initialize PLL */
//	writew(0x3002, SYNCR);

#ifdef BACKLIGHT_ON
	/* enable backlight */
	writew((1 << 5), DDRE);
	val = readw(PORTE0);
	writew(val &= ~(1 << 5), PORTE0);
#endif

	/* initialize chip selects */
	init_cs();
	flash_init();
	uart_init();


	spi_init();

	pmic_init();

	clockchip_init();

	hw_code = adc_read_hwcode();
	display_init(hw_code);

	keypad_init();
	pit_init();



	/* initialize periodic interrupt */
	


//	writew(0xcc00, SYNCR);

	/* cpu freq *4 */
//	writew(0xf008, SYNCR);

	/* default value */
//	writew(0x7008, SYNCR); 
}
