#ifndef _DISPLAY_H
#define _DISPLAY_H


void display_enable_large_font(bool on);
void display_clear(void);
void display_init(uint8_t hw_code);
void display_set_cursor(uint8_t line, uint8_t pos);
void display_enable_cursor(bool on);
void display_set_symbol(uint8_t symbol, bool on);
void display_set_batt_level(uint8_t level);
void display_set_rx_level(uint8_t level);
void display_puts(char *s, uint8_t line);
int display_printf(uint8_t line, const char *fmt, ...);

#define SYMB_ANTENNA	(0xc0 | 3)
#define SYMB_RXLEV1	(0xc0 | 0)
#define SYMB_RXLEV2	(0xd0 | 4)
#define SYMB_RXLEV3	(0xd0 | 2)
#define SYMB_RXLEV4	(0xd0 | 0)
#define SYMB_RXLEV5	(0xe0 | 4)
#define SYMB_RXLEV6	(0xe0 | 2)
#define SYMB_UPARROW	(0xf0 | 4)
#define SYMB_BELL	(0xf0 | 0)
#define SYMB_NOBELL	(0xf0 | 1)
#define SYMB_HANDSET	(0x90 | 3)
#define SYMB_HOOK	(0x90 | 2)
#define SYMB_ONE	(0x90 | 1)
#define SYMB_TWO	(0x90 | 0)
#define SYMB_RARROW1	(0x80 | 0)
#define SYMB_RARROW2	(0x80 | 1)
#define SYMB_HORIZBARS	(0xa0 | 0)
#define SYMB_MAIL	(0x80 | 4)
#define SYMB_SPKR_MUTE	(0x90 | 4)
#define SYMB_TAPE	(0xa0 | 4)
#define SYMB_BATT	(0xb0 | 0)
#define SYMB_BATTLEV1	(0xb0 | 2)
#define SYMB_BATTLEV2	(0xb0 | 4)
#define SYMB_BATTLEV3	(0xb0 | 1)

#endif
