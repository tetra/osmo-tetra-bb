#ifndef _PMIC_H
#define _PMIC_H

void pmic_init(void);
void pmic_transmit(void);
uint16_t pmic_read_voltage(void);

#endif
