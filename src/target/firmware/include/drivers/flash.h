#ifndef _FLASH_H
#define _FLASH_H

void flash_init(void);
uint32_t flash_prepare_addr(uint32_t addr);
void flash_read(uint32_t addr, void *dest, uint16_t len);

#endif
