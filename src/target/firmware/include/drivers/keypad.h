#ifndef _KEYPAD_H
#define _KEYPAD_H

enum key_codes {
	KEY_ANSWER = 0,
	KEY_STAR,
	KEY_0,
	KEY_HASH,
	KEY_DOWN,
	KEY_7,
	KEY_8,
	KEY_9,
	KEY_MODE,
	KEY_4,
	KEY_5,
	KEY_6,
	KEY_HANGUP,
	KEY_1,
	KEY_2,
	KEY_3,
	KEY_RIGHT_SB,
	KEY_MENU,
	KEY_UNUSED,		/* probably external PTT input */
	KEY_UP,
	KEY_VOLUP,
	KEY_VOLDOWN,
	KEY_LEFT_SB,
	KEY_PTT,
	KEY_PWRBTN,		/* this key is connected to the PMIC */
	KEY_INV = 0xFF
};

enum key_states {
	RELEASED,
	PRESSED
};

void keypad_emit_key(uint8_t key, uint8_t state);
void keypad_init(void);
void keypad_poll(void);

#endif
