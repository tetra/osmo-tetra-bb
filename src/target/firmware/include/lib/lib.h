#ifndef _LIB_H
#define _LIB_H

void *memcpy(void *dest, const void *src, size_t n);
void *memmove(void *dst, const void *src, size_t n);
int memcmp(const void *vl, const void *vr, size_t n);
int isprint(unsigned char c);
int strcmp(const char* s1, const char* s2);
uint32_t atoh(char *ap);
int atoi(const char* str);
uint32_t pow(uint8_t base, uint8_t power);
char *strchr(const char *s, const char c);
char *strstr(register char *string, char *substring);
size_t strlen(char *s);
int printf(const char *fmt, ...);
void delay_ms(unsigned int ms);
void delay_us(unsigned int us);
void dump_mem(uint8_t *dat, int len);

#endif
