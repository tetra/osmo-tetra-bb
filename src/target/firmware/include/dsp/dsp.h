#ifndef _DSP_H
#define _DSP_H

#define HI_STEP_RES	2400000		/* 2.4 MHz per step */
#define FREQ_STEP_SIZE	6250		/* 6.250 kHz channel spacing */
#define MIN_FREQ	401700000UL
#define MAX_FREQ	439900000UL

void dsp_init_after_2nd_image(uint32_t freq);

void dsp_read_memory(uint8_t mem, uint32_t addr);

void dsp_set_frequency(uint32_t freq);
void dsp_set_params(uint32_t freq);
void dsp_init(void);
void dsp_write_word(uint8_t *data);
void dsp_keypad_beep(uint8_t beeptype);
void dsp_start_tx(void);
void dsp_end_tx(void);

enum dsp_mem_type {
	DSP_P_MEM = 0,
	DSP_X_MEM,
	DSP_Y_MEM,
	DSP_CACHE_MEM,
	DSP_REG_MEM,
};


#endif
