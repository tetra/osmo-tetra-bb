#ifndef _DSP_IMAGE_H
#define _DSP_IMAGE_H

struct dsp_section {
	uint32_t addr;
	uint16_t len;
};

uint32_t dsp_find_version(uint32_t start_addr);
uint32_t dsp_find_bootstrap(void);
uint32_t dsp_find_image_table(void);
uint32_t dsp_find_data_section(void);
void dsp_read_image_table(struct dsp_section *section);

#endif
