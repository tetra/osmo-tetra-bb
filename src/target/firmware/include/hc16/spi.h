#ifndef _SPI_H
#define _SPI_H

// If this bit in the length field is set, we only send one byte instead of the last word
#define TRANSFER_END_ON_BYTE	(1 << 15)

#define ADDAG_CS	0
#define RF_CS		1
#define PMIC_CS		2

void spi_send(uint16_t *val, uint8_t len, uint8_t cs);
void spi_init(void);

#endif
