#ifndef _MEM_H
#define _MEM_H

//uint16_t paged_readw(uint8_t page, uint16_t addr);
//void paged_writew(uint8_t page, uint16_t addr, uint16_t val);
uint16_t readw_p(uint32_t addl);
void writew_p(uint16_t val, uint32_t addl);

//uint8_t paged_readb(uint8_t page, uint16_t addr);
//void paged_writeb(uint8_t page, uint16_t addr, uint8_t val);
uint8_t readb_p(uint32_t addl);
void writeb_p(uint8_t val, uint32_t addl);

void readmem(uint32_t src, void *dest, uint16_t len);

#define PAGEBYTE(x)  ((uint8_t *) &(x))[1]
#define MEMWORD(x)  ((uint16_t *) &(x))[1]

// get bytes from U32
//#define BYTE0(x)  ((uint8_t *) &(x))[0]
//#define BYTE1(x)  ((uint8_t *) &(x))[1]
//#define BYTE2(x)  ((uint8_t *) &(x))[2]
//#define BYTE3(x)  ((uint8_t *) &(x))[3]

#define WORD1(x)  ((uint16_t *) &(x))[0]
#define WORD0(x)  ((uint16_t *) &(x))[1]

#define BYTE0(x)  ((uint8_t *) &(x))[3]
#define BYTE1(x)  ((uint8_t *) &(x))[2]
#define BYTE2(x)  ((uint8_t *) &(x))[1]
#define BYTE3(x)  ((uint8_t *) &(x))[0]

#define __arch_getb(a)			(*(volatile unsigned char *)(a))
#define __arch_getw(a)			(*(volatile unsigned short *)(a))
#define __arch_getl(a)			(*(volatile unsigned int *)(a))

#define __arch_putb(v,a)		(*(volatile unsigned char *)(a) = (v))
#define __arch_putw(v,a)		(*(volatile unsigned short *)(a) = (v))
#define __arch_putl(v,a)		(*(volatile unsigned int *)(a) = (v))

#define __raw_writeb(v,a)		__arch_putb(v,a)
#define __raw_writew(v,a)		__arch_putw(v,a)
#define __raw_writel(v,a)		__arch_putl(v,a)

#define __raw_readb(a)			__arch_getb(a)
#define __raw_readw(a)			__arch_getw(a)
#define __raw_readl(a)			__arch_getl(a)

#define writeb(v,a)			__arch_putb(v,a)
#define writew(v,a)			__arch_putw(v,a)
#define writel(v,a)			__arch_putl(v,a)

#define readb(a)			__arch_getb(a)
#define readw(a)			__arch_getw(a)
#define readl(a)			__arch_getl(a)


#endif /* _MEM_H */
