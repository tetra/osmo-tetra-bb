#ifndef _REGS_H
#define _REGS_H

/* System Protection Control Register */
#define SYPCR		0xFFA20

#define PICR		0xFFA22 /* Periodic Interrupt Control Register */
#define PITR		0xFFA24 /* Periodic Interrupt Timer Register */

#define RAMMCR 		0xFFB00 /* RAM Module Configuration Register */
#define RAMBAH 		0xFFB04 /* RAM Array Base Address Register High */
#define RAMBAL		0xFFB06 /* RAM Array Base Address Register Low */

#define GPTMCR		0xFF900 /* GPT Module Configuration Register */
#define GPTICR		0xFF904 /* GPT Interrupt Configuration Register */

#define TMSK1		0xFF920 /* Timer Mask Register 1 */
#define CFORC		0xFF924 /* Compare Force Register */

#define SIMCR		0xFFA00 /* SIM Module Configuration Register */
#define QSMCR		0xFFC00 /* QSM Module Configuration Register */
#define QILR		0xFFC04 /* QSM Interrupt Level Register */
#define QIVR		0xFFC05 /* QSM Interrupt Vector Register */

#define ADCMCR		0xFF700 /* ADC Module Configuration Register */

#define ADCTL0		0xFF70A /* ADC Control Register 0 */
#define ADCTL1		0xFF70C /* ADC Control Register 1 */
#define ADCSTAT		0xFF70E /* ADC Status Register */
#define RJURR7		0xFF71E /* Right-Justified Unsigned Result Register 7 */

#define SPCR0		0xFFC18 /* QSPI Control Register 0 */
#define SPCR_MSTR	(1 << 15)

#define SPCR1		0xFFC1A /* QSPI Control Register 1 */
#define SPCR_SPE	(1 << 15)

#define SPCR2		0xFFC1C /* QSPI Control Register 2 */
#define SPCR3		0xFFC1E /* QSPI Control Register 3 */
#define SPCR3_HALT	(1 << 0)

#define SPSR		0xFFC1F /* QSPI Status Register */
#define SPSR_SPIF	(1 << 7)

#define QSPI_CR		0xFFD40

#define SPI_CR(n)	(0xFFD40 + n) /* QSPI Command RAM Byte 0 - 15 */
#define PCS(n)		(1 << n)
#define CR_CONT		(1 << 7)
#define CR_BITSE	(1 << 6)
#define CR_DT		(1 << 5)
#define CR_DSCK		(1 << 4)
#define SPI_RR(n)	(0xFFD00 + (n*2))
#define SPI_TR(n)	(0xFFD20 + (n*2))

#define QSMCR		0xFFC00

/* SCCI definitions */
#define MDDR		0xFFC0A

#define SCSR_TDRE	(1 << 8)
#define SCSR_TC		(1 << 7)
#define SCSR_RDRF	(1 << 6)

#define SCCR0		0xFFC08

#define SCCR1		0xFFC0A
#define	SCCR_TE		(1 << 3)
#define	SCCR_RE		(1 << 2)

#define SCSR		0xFFC0C
#define SCDR		0xFFC0E

/* Ports */

#define PORTADA		0xFF707
#define PORTC		0xFFA40
#define PORTE0		0xFFA10
#define PORTE1		0xFFA12
#define DDRE		0xFFA14
#define PEPAR		0xFFA16
#define PORTF0		0xFFA18
#define PORTF1		0xFFA1A
#define DDRF		0xFFA1C
#define PFPAR		0xFFA1E

#define DDRGP		0xFF906
#define PORTGP		0xFF907

/* Port QS */
#define PORTQS		0xFFC14
#define PQSPAR		0xFFC16	/* PORT QS Pin Assignment Register */
#define DDRQS		0xFFC17 /* PORT QS Data Direction Register */

/* PWM Control Register C */
#define PWMC		0xFF924
#define LED_GREEN	(1 << 0)
#define LED_RED		(1 << 1)

#define SYNCR		0xFFA04 /* Clock Synthesizer Control Register */

#define CSPAR0		0xFFA44 /* Chip-Select Pin Assignment Register 0 */
#define CSPAR1		0xFFA46 /* Chip-Select Pin Assignment Register 1 */
#define CSBARBT		0xFFA48 /* Chip-Select Base Address Register Boot */
#define CSORBT		0xFFA4A /* Chip-Select Option Register Boot */
#define CSBAR0		0xFFA4C /* Chip-Select Base Address Register 0 */
#define CSOR0		0xFFA4E /* Chip-Select Option Address Register 0 */
#define CSBAR1		0xFFA50 /* Chip-Select Base Address Register 1 */
#define CSOR1		0xFFA52 /* Chip-Select Option Address Register 1 */
#define CSBAR2		0xFFA54 /* Chip-Select Base Address Register 2 */
#define CSOR2		0xFFA56 /* Chip-Select Option Address Register 2 */
#define CSBAR3		0xFFA58 /* Chip-Select Base Address Register 3 */
#define CSOR3		0xFFA5A /* Chip-Select Option Address Register 3 */
#define CSBAR4		0xFFA5C /* Chip-Select Base Address Register 4 */
#define CSOR4		0xFFA5E /* Chip-Select Option Address Register 4 */
#define CSBAR5		0xFFA60 /* Chip-Select Base Address Register 5 */
#define CSOR5		0xFFA62 /* Chip-Select Option Address Register 5 */
#define CSBAR6		0xFFA64 /* Chip-Select Base Address Register 6 */
#define CSOR6		0xFFA66 /* Chip-Select Option Address Register 6 */
#define CSBAR7		0xFFA68 /* Chip-Select Base Address Register 7 */
#define CSOR7		0xFFA6A /* Chip-Select Option Address Register 7 */
#define CSBAR8		0xFFA6C /* Chip-Select Base Address Register 8 */
#define CSOR8		0xFFA6E /* Chip-Select Option Address Register 8 */
#define CSBAR9		0xFFA70 /* Chip-Select Base Address Register 9 */
#define CSOR9		0xFFA72 /* Chip-Select Option Address Register 9 */
#define CSBAR10		0xFFA74 /* Chip-Select Base Address Register 10 */
#define CSOR10		0xFFA76 /* Chip-Select Option Address Register 10 */

#endif /* _REGS_H */
