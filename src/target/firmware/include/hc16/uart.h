#ifndef _UART_H
#define _UART_H

void uart_init(void);
void uart_putdata(uint8_t *data, int len);
void putc(char c);
char getchar(void);
void puts(const char *s);

#endif
