#ifndef _INTERRUPTS_H
#define _INTERRUPTS_H

void pit_init(void);

static inline void enable_interrupts(void)
{
	asm("andp #0FF1Fh");
}

static inline void disable_interrupts(void)
{
	asm("orp #00E0h");
}

#endif
