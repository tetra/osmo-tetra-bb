#ifndef _ASM_H
#define _ASM_H

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned short size_t;
typedef unsigned long uint32_t;
typedef char bool;
enum { false, true };
#define NULL 0

#define ARRAY_SIZE(array) (sizeof(array) / sizeof(array[0]))

/* imports from hello_world.s */
//extern void putc(unsigned char c);
//extern int puts(const char* c);
//extern void phex(unsigned int c, unsigned int len);
extern void memdump(void *ptr);

#ifdef DEBUG
#define dprintf(format, args...) printf(format, args)
#define dprintf(x) printf x
#define dputc(x) putc(x)
#define dputs(x) puts(x)
#define dphex(x,y) phex(x,y)
#else
#define dprintf(format, args...)
#define dputc(x)
#define dputs(x)
#define dphex(x,y)
#endif

#endif /* _ASM_H */
