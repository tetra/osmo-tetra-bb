/* Interrupt handlers and periodic interrupt timer
 * 
 * This file is part of osmo-tetra-bb.
 *
 * Copyright (C) 2021 by Steve Markgraf <steve@steve-m.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 * osmo-tetra-bb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * osmo-tetra-bb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm.h>
#include <hc16/mem.h>
#include <hc16/regs.h>
#include <hc16/uart.h>
#include <drivers/display.h>
#include <drivers/keypad.h>
#include <lib/lib.h>
#include <interrupts.h>

void pit_init(void)
{
	/* Enable the periodic interrupt timer */
	writew(0x0638, PICR);
	writew(0x0101, PITR);	//writew(0x0110, PITR);
}

void pit_interrupt(void)
{
	static uint8_t cnt = 0;
	static uint16_t foo = 0;
//	printf("Interrupt %s!\n", u32_to_hex(foo));
	

	foo++;

	if (foo > 40) {
		dprintf("Battery voltage: %d mV\n", pmic_read_voltage());
//		display_set_rx_level(cnt++);
		if (cnt == 7)
			cnt = 0;

//		display_puts("test", 3);
		foo = 0;
	}

	keypad_poll();

//	if (foo & 1)
//		writew(LED_GREEN, PWMC);
//	else
//		writew(0, PWMC);
	
}

void unhandled_interrupt(void)
{
	writew(LED_RED, PWMC);
	puts("Unhandled Interrupt!\n");
	disable_interrupts();
}

void swi_interrupt(void)
{
	puts("Software interrupt!\n");
	disable_interrupts();
}
