/* Flash driver for Motorola d700/MTH300
 *
 * This file is part of osmo-tetra-bb.
 *
 * Copyright (C) 2021-2022 by Steve Markgraf <steve@steve-m.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 * osmo-tetra-bb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * osmo-tetra-bb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm.h>
#include <hc16/mem.h>
#include <drivers/flash.h>
#include <lib/lib.h>
#include <hc16/regs.h>

/* Set flash page 0 - 3 */
void flash_set_page(uint8_t page)
{
	static uint8_t lastpage = 0xff;
	uint8_t val;

	if (page == lastpage)
		return;

	val = readb(PORTGP) & ~0xc0;
	val |= (page << 6);
	writeb(val, PORTGP);
	lastpage = page;
}

void flash_init(void)
{
	writeb(0xc0, DDRGP);
	flash_set_page(0);
}

uint32_t flash_prepare_addr(uint32_t addr)
{
	/* the lower 512KB of the flash can be read directly */
	if (PAGEBYTE(addr) < 0x8)
		return addr;

	/* the upper 1024KB of the flash are mapped into a 256K window */

	if (PAGEBYTE(addr) < 0x0C) {
		flash_set_page(0);
	} else if (PAGEBYTE(addr) < 0x10) {
		flash_set_page(1);
		PAGEBYTE(addr) -= 0x04;
	} else if (PAGEBYTE(addr) < 0x14) {
		flash_set_page(2);
		PAGEBYTE(addr) -= 0x08;
	} else {
		flash_set_page(3);
		PAGEBYTE(addr) -= 0x0c;
	}

	return addr;
}

void flash_read(uint32_t addr, void *dest, uint16_t len)
{
	readmem(flash_prepare_addr(addr), dest, len);
}
