#include <asm.h>
#include <hc16/mem.h>
#include <hc16/spi.h>
#include <lib/lib.h>
#include <drivers/rf.h>
#include <hc16/regs.h>

void rf_init(void)
{
	uint16_t rf1[] = { 0x0e00, 0x6e08, 0x43f2 };
	//uint16_t rf1[] = {0x0e00, 0x6800, 0x43f2 }; //patched for tx
	spi_send(rf1, ARRAY_SIZE(rf1), RF_CS);
}

void rf_query(void)
{
	int i;
	static uint16_t response[16] = {0};
	uint16_t val;
	uint16_t rf_query[] = {//mosi	miso
				0x0E04,	//0xFF82
				0xF900,	//0x2A8C
				0x0018,	//0x7FFF
				0x1F21,	//0x7F1B
				0x507C,	//0x857F
				0x3410,	//0x7F55
				0x4440,	//0x822A
				0x0018,	//0x8C7F
				0x2800,	//0xFF7F		//2800		// what?
				0x3ABB,	//0x1B85		//03bb
				0x5180,	//0x7F7F		//5100
				0xA31D,	//0x5582		//031d
				0x3038,	//0x2A8C		//3001
				0x0410, //0x2410,		//0x7FFF		//0210	Saw 4010 during call
				0x0F60,	//0x7F1B		//0f65		0f64, was 0f60
				0x0000,	//0x857F		//
			};

	spi_send(rf_query, ARRAY_SIZE(rf_query), RF_CS);

	for (i = 0; i < ARRAY_SIZE(response); i++) {
		val = readw(SPI_RR(i));
		if (val != response[i]) {
		
			//if (i == 9)
				printf("RF %d has changed: %04x\n", i, val);


			response[i] = val;
		}
	}

//	delay_ms(1);
}
