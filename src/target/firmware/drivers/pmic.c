#include <asm.h>
#include <hc16/mem.h>
#include <drivers/keypad.h>
#include <drivers/pmic.h>
#include <lib/lib.h>
#include <hc16/spi.h>
#include <hc16/regs.h>

/* This function powers off the phone */
void pmic_poweff()
{
	/* configure PORTGP5 as output */

	/* pull port GP5 low */
	
	//PortGP0 is also involved?!
	
	//mw ff906 de00 and mw ff906 deff both work

}

void pmic_init(void)
{
	int i;
	uint16_t val;
	uint16_t init[] = {	0x4000, 0x0000,
				0xA00B, 0x0400,
				0x8197, 0x0F20,
//				0x8197, 0x0F20,
				0x0000, 0x0000,
				0xC7F1, 0xE600,
				0x4000, 0x0000,
				0xA00B, 0x0000,
				0x2000, 0x0000,
				0xE000, 0x0000,
				0x6000, 0x0000,		// can be used to read battery voltage in LSB
				0x9000, 0x0000,
				0x1000, 0x0000,
				0xD000, 0x0000,
				0x5000, 0x0000,
				0xBD85, 0xB080,
				0x3000, 0x0000,
				0xF100, 0x0000,
				0x7000, 0x0000,
		 };

	for (i = 0; i < ARRAY_SIZE(init); i += 2) {
		spi_send(&init[i], 2, PMIC_CS);
		printf("SPI: %04x, %04x\n", readw(SPI_RR(0)), readw(SPI_RR(1)));
		delay_ms(1);
	}

	/* enable PMIC interrupts */
	val = readw(PFPAR);
	val |= (1 << 6);
	writew(val, PFPAR);
}

void pmic_transmit(void)
{
	int i;
	uint16_t val;
	uint16_t tx[] = {	0xf180, 0x1320,
				0xf1c0, 0x1320,
				0x7000, 0x0000,
				0xf100, 0x0c20,
				0x8197, 0x0f20,
				0xbdf5, 0x5082,

		 };

	for (i = 0; i < ARRAY_SIZE(tx); i += 2) {
		spi_send(&tx[i], 2, PMIC_CS);
		delay_ms(1);
	}

}

uint16_t pmic_read_voltage(void)
{
	int i;
	uint16_t voltage;
	uint16_t read_adc[] = {	0xf180, 0x2120,
				0xf1c0, 0x2120,
				0x7000, 0x0000,
				0xf100, 0x1420, };

	for (i = 0; i < ARRAY_SIZE(read_adc); i += 2) {
		spi_send(&read_adc[i], 2, PMIC_CS);
		delay_ms(1);
	}

	voltage = readw(SPI_RR(1)) >> 6;
	voltage = 4100 + (voltage * 25);

	return voltage;
}

void pmic_power_off(void)
{
//	uint16_t off[] = { 0x4000, 0x2445 };
//	printf("Calling power off\n");
//	spi_send(off, sizeof(off), PMIC_CS);
}

#define PWR_BTN_PRESSED		(1 << 8)

bool pmic_read_pwrbutton(void)
{
	uint16_t read_status[] = { 0x4000, 0x0000 };
	spi_send(read_status, ARRAY_SIZE(read_status), PMIC_CS);

	return !(readw(SPI_RR(1)) & PWR_BTN_PRESSED);
}

void pmic_interrupt(void)
{
	static bool last_pwrbtn_state = 0;
	bool cur_pwrbtn_state;
	uint16_t clr_int[] = { 0xcffb, 0xe6f0 };

	cur_pwrbtn_state = pmic_read_pwrbutton();
	if (last_pwrbtn_state != cur_pwrbtn_state) {
		keypad_emit_key(KEY_PWRBTN, cur_pwrbtn_state);
		last_pwrbtn_state = cur_pwrbtn_state;
	}

	/* acknowledge the interrupt */
	spi_send(clr_int, ARRAY_SIZE(clr_int), PMIC_CS);
}
