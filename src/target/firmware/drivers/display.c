/* Display driver for Motorola d700/MTH300
 * 
 * This file is part of osmo-tetra-bb.
 *
 * Copyright (C) 2021-2022 by Steve Markgraf <steve@steve-m.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 * osmo-tetra-bb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * osmo-tetra-bb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm.h>
#include <hc16/regs.h>
#include <hc16/mem.h>
#include <lib/lib.h>
#include <lib/mini-printf.h>
#include <drivers/display.h>

#define DISPLAY_LINES		4
#define DISPLAY_CHARS		12

#define DISPLAY_CMD_ADDR	0xE0000
#define DISPLAY_DATA_ADDR	0xE0001

/* display commands */
#define DISPLAY_CMD_CLEAR	0x01
#define DISPLAY_CMD_HOME	0x02
#define DISPLAY_CMD_ON		0x08
#define DISPLAY_CMD_DDRAM	0x80
/* lower two bits of instruction 0x4f control contrast */

#define DISPLAY_BUSY		(1 << 7)

bool g_cursor_enabled = false;
uint8_t g_cursor_line = 0;
uint8_t g_cursor_pos = 0;

/* Wait while display is busy, return current address counter value */
void display_wait_busy(void)
{
	uint8_t val;

	do {
		val = readb_p(DISPLAY_CMD_ADDR) & 0xf0;
		val |= (readb_p(DISPLAY_CMD_ADDR) >> 4);
	} while (val & DISPLAY_BUSY);
}

void display_cmd(uint8_t dat)
{
	display_wait_busy();
	writeb_p(dat & 0xf0, DISPLAY_CMD_ADDR);
	writeb_p((dat << 4), DISPLAY_CMD_ADDR);
	display_wait_busy();
}

void display_data(uint8_t dat)
{
	display_wait_busy();
	writeb_p(dat & 0xf0, DISPLAY_DATA_ADDR);
	writeb_p(dat << 4, DISPLAY_DATA_ADDR);
	display_wait_busy();
}

/* Set write address to beginning of display line 0 - 3 */
void display_set_pos(uint8_t line, uint8_t pos)
{
	display_cmd(line > 1 ? 0xc0 : 0xc1);
	display_cmd((line & 1 ? 0xe0 : 0xf0) + pos);
}

void display_set_cursor(uint8_t line, uint8_t pos)
{
	display_set_pos(line, pos);
	g_cursor_line = line;
	g_cursor_pos = pos;
}

void display_enable_cursor(bool on)
{
	display_cmd(on ? (DISPLAY_CMD_ON | 0x04) : DISPLAY_CMD_ON);
	g_cursor_enabled = on;
}

void display_clear_symbols(void)
{
	int i;

	display_cmd(0x88);

	for (i = 0; i < 24; i++)
		display_data(0x00);
}

/* this combines the second and third line and enables a larger font for this line */
void display_enable_large_font(bool on)
{
	display_cmd(on ? 0x3a : 0x38);
}

void display_clear(void)
{
	display_cmd(DISPLAY_CMD_CLEAR);
	delay_ms(1);
	display_clear_symbols();
}

void display_init(uint8_t hw_code)
{
	display_cmd(0x03);
	display_cmd(0x16);	// with 0x1f you can let a whole row blink with the cursor
	display_enable_large_font(false);
	display_cmd(0x1c);

	/* This command is some sort of contrast setting,
	 * which differs from HW code Ac to Bc.. */
	display_cmd(hw_code == 0xbc ? 0x4a : 0x4f);

	display_cmd(DISPLAY_CMD_ON);
	display_clear();
}

void display_set_symbol(uint8_t symbol, bool on)
{
	static uint8_t symbol_state[8] = { 0 };
	uint8_t addr = 0x80 | (symbol >> 4);
	uint8_t data = symbol & 0x0f;
	uint8_t i = addr - 0x88;
	uint8_t val;

	if (on)
		val = symbol_state[i] | (1 << data);
	else
		val = symbol_state[i] & ~(1 << data);

	/* check if there is a change */
	if (symbol_state[i] == val)
		return;

	symbol_state[i] = val;

	display_cmd(addr);
	display_data(val);

	/* restore the cursor */
	if (g_cursor_enabled)
		display_set_pos(g_cursor_line, g_cursor_pos);
}

/* Set battery level 0 - 3 */
void display_set_batt_level(uint8_t level)
{
	display_set_symbol(SYMB_BATT, true);
	display_set_symbol(SYMB_BATTLEV1, level > 0);
	display_set_symbol(SYMB_BATTLEV2, level > 1);
	display_set_symbol(SYMB_BATTLEV3, level > 2);
}

/* Set rx level 0 - 6 */
void display_set_rx_level(uint8_t level)
{
	display_set_symbol(SYMB_RXLEV1, level > 0);
	display_set_symbol(SYMB_RXLEV2, level > 1);
	display_set_symbol(SYMB_RXLEV3, level > 2);
	display_set_symbol(SYMB_RXLEV4, level > 3);
	display_set_symbol(SYMB_RXLEV5, level > 4);
	display_set_symbol(SYMB_RXLEV6, level > 5);
}

void display_puts(char *s, uint8_t line)
{
	uint8_t i;

	display_set_pos(line, 0);

	while (*s != 0)
		display_data(*s ? *s++ : ' ');

	/* we need to restore the cursor, as the position was changed by our written data */
	if (g_cursor_enabled)
		display_set_pos(g_cursor_line, g_cursor_pos);
}

int display_printf(uint8_t line, const char *fmt, ...)
{
	char buf[16];
	va_list va;
	int len;

	va_start(va, fmt);
	len = mini_vsnprintf(buf, sizeof(buf), fmt, va);
	va_end(va);

	display_puts(buf, line);

	return len;
}
