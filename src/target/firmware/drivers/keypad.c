/* Keypad driver for Motorola d700/MTH300
 * 
 * This file is part of osmo-tetra-bb.
 *
 * Copyright (C) 2021 by Steve Markgraf <steve@steve-m.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 * osmo-tetra-bb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * osmo-tetra-bb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm.h>
#include <hc16/regs.h>
#include <hc16/mem.h>
#include <hc16/uart.h>
#include <lib/lib.h>
#include <drivers/keypad.h>

/* column inputs are connected to PORTADA1-4 */
#define COL1		(1 << 1)
#define COL2		(1 << 2)
#define COL3		(1 << 3)
#define COL4		(1 << 4)

#define COL_MASK	(COL1 | COL2 | COL3 | COL4)

/* row 1-5 outputs are connected to PORTC2-6 */
#define ROW1		(1 << 2)	/* *, 0, # and green phone key */
#define ROW2		(1 << 3)	/* 7, 8, 9 and down arrow */
#define ROW3		(1 << 4)	/* 4, 5, 6 and mode selector key */
#define ROW4		(1 << 5)	/* 1, 2, 3 */
#define ROW5		(1 << 6)	/* up arrow, menu, speaker and right display key */

#define PC_ROW_MASK	(ROW1 | ROW2 | ROW3 | ROW4 | ROW5)

/* Row 6 is connected to PORTF7 */
#define ROW6		(1 << 7)	/* left display key, volume up, down, PTT key */

#define ROW_CNT		6
#define COL_CNT		4

void keypad_emit_key(uint8_t key, uint8_t state)
{
	printf("key=%u %s\n", key, state == PRESSED ? "pressed" : "released");
	key_handler(key, state);
}

void keypad_init(void)
{
	/* initialize output ports used for keypad matrix */
	writew(0, PORTC);
	writew(readw(DDRF) | ROW6, DDRF);
	writew(readw(PORTF0) & ~ROW6, PORTF0);
}

void keypad_scan_key(void)
{
	static uint8_t key_state[ROW_CNT] = { 0 };
	uint8_t val, active_col, active_row = ROW1;
	uint16_t portf = readw(PORTF0);
	uint8_t i, j, diff;

	/* we need to set all rows high, except one, and then scan if the column is still low */
	writew(PC_ROW_MASK, PORTC);
	writew(portf | ROW6, PORTF0);

	/* now scan all rows */
	for (i = 0; i < ROW_CNT; i++) {
		if (i < ROW_CNT-1) {
			/* Scan rows attached to port C */
			writew(PC_ROW_MASK & ~active_row, PORTC);
			active_row <<= 1;
		} else {
			/* Scan the row attached to port F */
			writew(PC_ROW_MASK, PORTC);
			writew(portf & ~ROW6, PORTF0);
		}

		val = ~(readb(PORTADA) & COL_MASK);
		diff = val ^ key_state[i];

		if (diff) {
			key_state[i] = val;
			active_col = COL1;
			for (j = 0; j < COL_CNT; j++) {
				if (diff & active_col)
					keypad_emit_key((i << 2) | j, !!(active_col & val));

				active_col <<= 1;
			}
		}
	}

	/* set all rows low again */
	writew(0, PORTC);
}

void keypad_poll(void)
{
	static uint8_t any_key_pressed = 0;

	/* check if any key is pressed */
	if ((readb(PORTADA) & COL_MASK) != COL_MASK) {
		keypad_scan_key();
		any_key_pressed = 1;
		return;
	}

	/* if at the last scan there was at least still one key pressed,
	 * we need to propagate the release of the key(s) */
	if (any_key_pressed) {
		keypad_scan_key();
		any_key_pressed = 0;
	}
}
