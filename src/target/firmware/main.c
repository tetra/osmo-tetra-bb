/* osmo-tetra-bb main program
 *
 * This file is part of osmo-tetra-bb.
 *
 * Copyright (C) 2015-2022 by Steve Markgraf <steve@steve-m.de>
 *
 * Some of the code is borrowed from the osmocom-bb rssi app:
 * Copyright (C) 2012 by Andreas Eversberg <jolly@eversberg.eu>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 * osmo-tetra-bb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * osmo-tetra-bb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm.h>
#include <hc16/mem.h>
#include <hc16/regs.h>
#include <init.h>

#include <drivers/display.h>
#include <drivers/flash.h>
#include <drivers/keypad.h>
#include <lib/mini-printf.h>
#include <lib/lib.h>
#include <hc16/uart.h>
#include <dsp/dsp.h>

#include <interrupts.h>

enum key_codes key_code = KEY_INV;
bool key_pressed = false;
enum key_codes key_pressed_code;
unsigned long key_pressed_when;
unsigned int key_pressed_delay;

enum mode {
	MODE_MAIN,
	MODE_FREQ,
} mode = MODE_MAIN;

void print_cursor(void)
{
	puts("\r> ");
}

void dump_flash(void)
{
	uint32_t addr = 0x0;
	uint8_t fdata[32];

	disable_interrupts();

	do {
		flash_read(addr, fdata, sizeof(fdata));
		uart_putdata(fdata, sizeof(fdata));
		addr += sizeof(fdata);
	} while (addr < 0x180000);

//	enable_interrupts();
}

void dump_codeplug(void)
{
	uint32_t addr = 0xe0000;
	uint8_t fdata[32];

	disable_interrupts();

	do {
		readmem(addr, fdata, sizeof(fdata));
		uart_putdata(fdata, sizeof(fdata));
		addr += sizeof(fdata);
	} while (addr < 0xe8000);

//	enable_interrupts();
}

void dump_dsp_mem(void)
{
	uint32_t addr = 0x00000;

	do {
		dsp_read_memory(DSP_Y_MEM, addr++);
		delay_ms(5);
	} while (addr < 0xf0000);

}

void long_delay(void)
{
	delay_ms(320);
	delay_ms(320);
	delay_ms(320);
	delay_ms(320);
	delay_ms(320);
	delay_ms(320);
	delay_ms(320);
	delay_ms(320);
}

int execute_cmd(char *cmd)
{
	char *arg1 = strchr(cmd, ' ');
	char *arg2 = NULL;
	uint32_t argval1 = 0;
	uint32_t argval2 = 0;
	uint8_t data[3];
	uint16_t val;
	uint8_t page;

	if (strstr(cmd, "mw")) {
		if (arg1)
			arg2 = strchr(&arg1[1], ' ');
		
		if (arg1 && arg2) {
			argval1 = atoh(arg1) & 0xfffff;
			argval2 = atoh(arg2) & 0xffff;

			page = (argval1 >> 16) & 0xf;
			//puts("\nWriting page 0x");
			//puthex(page, 0);
			//puts(" addr 0x");
			//puthex(argval1, 0);
			//puts(" -> 0x");
			//puthex(argval2, 1);
			puts("\n");

			writew_p(argval2, argval1);
		} else
			printf("Missing argument\n");
	} else if (strstr(cmd, "mr")) {
		if (arg1) {
			argval1 = atoh(arg1) & 0xfffff;

			printf("\nReading 0x%lx -> 0x%04x\n", argval1, readw_p(argval1));
		} else
			printf("\nMissing argument\n");
	} else if (strstr(cmd, "bw")) {
		if (arg1)
			arg2 = strchr(&arg1[1], ' ');
		
		if (arg1 && arg2) {
			argval1 = atoh(arg1) & 0xfffff;
			argval2 = atoh(arg2) & 0xff;

			page = (argval1 >> 16) & 0xf;
			puts("\nWriting page 0x");
			//puthex(page, 0);
			puts(" addr 0x");
			//puthex(argval1, 0);
			puts(" -> 0x");
			//puthex(argval2, 1);
			puts("\n");

			writeb_p(argval2, argval1);
		} else
			printf("Missing argument\n");
	} else if (strstr(cmd, "br")) {
		if (arg1) {
			argval1 = atoh(arg1) & 0xfffff;

			page = (argval1 >> 16) & 0xf;
			puts("\nReading page 0x");
			//puthex(page, 0);
			puts(" addr 0x");
			//puthex(argval1, 0);
			puts(" -> 0x");
			//puthex(readb_p(argval1), 1);
			puts("\n");

		} else
			printf("\nMissing argument\n");
	} else if (strstr(cmd, "dsp")) {
		if (arg1) {
			argval1 = atoh(arg1);
			data[0] = BYTE1(argval1);
			data[1] = BYTE2(argval1);
			data[2] = BYTE3(argval1);
			//printf("Writing DSP Word: %s\n", u32_to_hex(argval1));
			printf("Writing DSP Word: %lx\n", 0xcafebabe);

	//		printf("B0: %02x, B1: %02x, B2: %02x, B3: %02x\n", BYTE0(argval1), BYTE1(argval1), BYTE2(argval1), BYTE3(argval1));

			dsp_write_word(data);

//		dsp_do_trick();
		} else
			printf("Missing argument\n");
	} else if (strstr(cmd, "freq")) {
		if (arg1) {
			val = atoi(arg1+1);

			printf("Setting Freq: %d from %s\n", val, arg1);
			dsp_init_after_2nd_image(val);

		} else
			printf("Missing argument\n");
	} else if (strstr(cmd, "dump")) {
		printf("Start!");
		long_delay();
		dump_flash();
		printf("End!");
	} else if (strstr(cmd, "plug")) {
		printf("Start!");
		long_delay();
		dump_codeplug();
		printf("End!");
	} else if (strstr(cmd, "tx1")) {
		dsp_start_tx();
	} else if (strstr(cmd, "dmem")) {
#if 0
		if (arg1) {
			argval1 = atoh(arg1);
			printf("Reading DSP memory addr: %lx\n", argval1);
			dsp_read_memory(DSP_P_MEM, argval1);
		} else
			printf("Missing argument\n");
#endif
		long_delay();
		long_delay();
		long_delay();
		long_delay();
		long_delay();
		long_delay();
		dump_dsp_mem();

	} else if (strstr(cmd, "end")) {
		dsp_end_tx();
	} else
		printf("\nUnknown command\n");

	return 0;
}

void console(void)
{
	char c;
	char cmdbuf[2][64];
	unsigned int i = 0, cur_buf = 0;
	int last_exec = -1;
	int escape;
	int j;
	unsigned int pos;

	print_cursor();

	while (1) {
		c = getchar();

		switch (c) {
		case '\r':
			putc('\n');

			//if (cmdbuf[cur_buf][i] != '\0') {
				/* execute the current command */
				execute_cmd(cmdbuf[cur_buf]);
				cur_buf = (cur_buf + 1) % (sizeof(cmdbuf)/sizeof(cmdbuf[0]));
				last_exec = cur_buf;
			//}

			print_cursor();
			i = 0;
			continue;

		case 0x7f:
			/* we reached the beginning of the line, don't echo */
			if (i == 0)
				continue;

			cmdbuf[cur_buf][i--] = '\0';
			puts("\b \b");
			break;

		case 0x1b:
			escape = 1;
			continue;
		case 0x5b:
			if (escape == 1) {
				/* expecting arrow key */
				escape = 2;
				continue;
			}
		/* handle cursor up/down: previous/next command */
		case 'A':
		case 'B':
			if (escape == 2) {
				/* no command was executed yet */
				if (last_exec < 0)
					continue;

		//		if (history_pos < 0) {
	//				history_pos += (c == 'A' ? 1 : -1);
//				}

				for (j = 0; j < strlen(cmdbuf[cur_buf]); j++)
					puts("\b \b");

				printf("last ex: %d\n", last_exec);

				cur_buf = (cur_buf + (c == 'A' ? 1 : -1)) % ARRAY_SIZE(cmdbuf);

				print_cursor();
				i = strlen(cmdbuf[cur_buf]);

				puts(cmdbuf[cur_buf]);
				continue;
			}
		case 'C':	/* cursor moves right */
			if (escape == 2) {
				if (i < strlen(cmdbuf[cur_buf])) {
					i++;
					puts("\033[1C");
				}
				escape = 0;
				continue;
			}
		case 'D':	/* cursor moves left */
			if (escape == 2) {
				if (i > 0) {
					i--;
					puts("\033[1D");
				}
				escape = 0;
				continue;
			}

		default:
			//printf("got key %02x, b is %d\n", c, '\b');
			if (i < sizeof(cmdbuf[0]) - 1) {
				cmdbuf[cur_buf][i++] = c;
				cmdbuf[cur_buf][i] = '\0';
			}
			else
				continue;
		}

		/* echo the character */
		if (isprint(c))
			putc(c);
	}
}

/* as we cannot go below 400 MHz, start editing at the the next digit */
uint8_t last_freq_edit_cursor = 1;
uint8_t cursor_pos = 1;

uint32_t frequency = 420125000; //420125000;
uint32_t old_frequency = 0;
uint32_t cur_talkgroup = 1;
uint32_t old_talkgroup = 0;

uint8_t current_item = 0;

void round_frequency(void)
{
	uint32_t mod = frequency % FREQ_STEP_SIZE;

	if (mod != 0) {
		if (mod > (FREQ_STEP_SIZE/2))
			frequency -= mod;
		else
			frequency += (FREQ_STEP_SIZE - mod);
	}

	if (frequency < MIN_FREQ)
		frequency = MIN_FREQ;
	else if (frequency > MAX_FREQ)
		frequency = MAX_FREQ;
}

void print_freq(void)
{
	uint16_t freq_mhz = frequency / 1000000;
	uint32_t freq_khz = frequency % 1000000;

	display_printf(0, " %d.%06ldM", freq_mhz, freq_khz);

	display_puts("\x7e", current_item);
	display_printf(1, " TG: %ld", cur_talkgroup);
	display_printf(2, " ISSI: 100");

	display_puts("        Edit", 3);
}

void update_screen(void)
{
	round_frequency();
	print_freq();

	if (mode == MODE_MAIN) {
		/* draw new selector */
		display_puts("\x7e", current_item);
		display_enable_cursor(false);
	}

	if (mode == MODE_FREQ) {
		/* clear cursor */
		//display_puts(" ", current_item);

		/* enter the edit mode */
		display_set_cursor(0, cursor_pos);
		display_enable_cursor(true);
		display_puts("Cancel Apply", 3);
	}
}

void increase_val()
{

	if (mode == MODE_MAIN) {
		display_puts(" ", current_item);

		if (current_item > 0)
			current_item--;

		display_puts("\x7e", current_item);
	}

	if (mode == MODE_FREQ) {
		uint8_t cpos = 9 - cursor_pos;
		if (cursor_pos > 3)
			cpos++;

		frequency += pow(10, cpos);
		update_screen();
	}
}

void decrease_val()
{
	if (mode == MODE_MAIN) {
		/* clear old selector */
		display_puts(" ", current_item);

		if (current_item < 2)
			current_item++;

		display_puts("\x7e", current_item);
	}

	if (mode == MODE_FREQ) {
		uint8_t cpos = 9 - cursor_pos;
		if (cursor_pos > 3)
			cpos++;

		frequency -= pow(10, cpos);
		update_screen();
	}
}

void handle_num(uint8_t num)
{
	uint32_t n;
	if (mode == MODE_FREQ) {
		n = pow(10, 9 - cursor_pos);
		frequency -= ((frequency/n) % 10) *n;
		frequency += n * num;

		if (cursor_pos < 10)
			cursor_pos++;

		/* skip over dot */
		if (cursor_pos == 4)
			cursor_pos++;

		update_screen();
	}
}

void key_handler(enum key_codes code, enum key_states state)
{
	printf("entering key handler");
	
	if (state != PRESSED) {
		key_pressed = false;
		return;
	}

	/* key repeat */
	if (!key_pressed) {
		key_pressed = true;
//		key_pressed_when = jiffies;
		key_pressed_code = code;
//		key_pressed_delay = HZ * 6 / 10;
	}

	key_code = code;
}

void handle_key_code(void)
{
	if (key_code == KEY_INV)
		return;

	/* special case for releasing PTT key */
//	if ((key_code == KEY_PTT) && (state == RELEASED))
//		dsp_end_tx();

//	if (state != PRESSED)
//		return;

	//dsp_keypad_beep(1);
	switch (key_code) {
	case KEY_0: handle_num(0); break;
	case KEY_1: handle_num(1); break;
	case KEY_2: handle_num(2); break;
	case KEY_3: handle_num(3); break;
	case KEY_4: handle_num(4); break;
	case KEY_5: handle_num(5); break;
	case KEY_6: handle_num(6); break;
	case KEY_7: handle_num(7); break;
	case KEY_8: handle_num(8); break;
	case KEY_9: handle_num(9); break;
	case KEY_RIGHT_SB:
		if (mode == MODE_MAIN) {
			/* switch to frequency editing mode */
			if (current_item == 0) {
				mode = MODE_FREQ;
				old_frequency = frequency;

				cursor_pos = last_freq_edit_cursor;
				update_screen();
			}
		} else if (mode == MODE_FREQ) {
			/* leave frequency editing mode and apply new freq */
			mode = MODE_MAIN;
			last_freq_edit_cursor = cursor_pos;
			update_screen();

			/* update DSP frequency */
			disable_interrupts();
			dsp_set_frequency(frequency);
			clockchip_init();
			dsp_init();
			//dsp_set_params(1);
			enable_interrupts();
			delay_ms(100);
			dsp_keypad_beep(1);
		}
		break;
	case KEY_LEFT_SB:
		if (mode == MODE_MAIN) {

		} else if (mode == MODE_FREQ) {
			/* leave frequency editing mode and restore old freq */
			mode = MODE_MAIN;
			frequency = old_frequency;
			update_screen();
		}
		break;
	case KEY_PWRBTN:
		pmic_power_off();
		break;
	case KEY_PTT:
	case KEY_ANSWER:
		dsp_start_tx();
		display_set_symbol(SYMB_ANTENNA, true);
		break;
	case KEY_HANGUP:
		dsp_end_tx();
		display_set_symbol(SYMB_ANTENNA, false);
		break;
	case KEY_STAR:
		dsp_init();
		break;

	case KEY_MODE:
		if (cursor_pos > 1)
			cursor_pos--;
		else
			cursor_pos = 10;

		/* in freq edit mode, skip over the dot */
		 if ((mode == MODE_FREQ) && cursor_pos == 4)
			cursor_pos--;

		display_set_cursor(0, cursor_pos);
		break;

	case KEY_MENU:
		if (cursor_pos < 10)
			cursor_pos++;
		else
			cursor_pos = 1;

		/* in freq edit mode, skip over the dot */
		 if ((mode == MODE_FREQ) && cursor_pos == 4)
			cursor_pos++;

		display_set_cursor(0, cursor_pos);
		break;
	case KEY_DOWN:
		decrease_val();
		break;
	case KEY_UP:
		increase_val();
		break;
	default:
		writew(0, PWMC);
		break;
	}

	key_code = KEY_INV;
}

void boot_screen(void)
{
	/* display boot screen */
	display_set_rx_level(6);
	display_set_symbol(SYMB_ANTENNA, true);
	display_enable_large_font(true);
	display_puts("   osmocom", 1);
	display_puts("  tetra-bb", 2);
	display_puts(" Loading DSP", 3);
}

/* Main Program */
int main(void)
{
	writew(LED_GREEN, PWMC);
	init();

	puts("\n\n\n\n\n");
	puts("osmo-tetra-bb booted, starting console\n");

	boot_screen();

	dsp_set_frequency(frequency);
//	dsp_init();

	display_enable_large_font(false);

	display_clear();
	enable_interrupts();

	update_screen();

//	dsp_keypad_beep(1);

//	console();

	while (1) {
		handle_key_code();
	}
}
