#include <asm.h>
#include <hc16/uart.h>
#include <lib/mini-printf.h>
#include <lib/lib.h>

void *memcpy(void *dest, const void *src, size_t n)
{
	size_t i;
	char *csrc = (char *)src;
	char *cdest = (char *)dest;

	for (i = 0; i < n; i++)
		cdest[i] = csrc[i];

	return dest;
}

void *memmove(void *dst, const void *src, size_t n)
{
	size_t i;
	char *csrc = (char *)src;
	char *cdest = (char *)dst;

	if (dst < src)
		return memcpy(dst, src, n);

	for (i = n; i > 0; i--)
		cdest[i-1] = csrc[i-1];

	return dst;
}

int memcmp(const void *vl, const void *vr, size_t n)
{
	const unsigned char *l=vl, *r=vr;
	for (; n && *l == *r; n--, l++, r++);
	return n ? *l-*r : 0;
}

int isprint(unsigned char c)
{
	return (c - ' ') < 0x5f;
}

int strcmp(const char* s1, const char* s2)
{
	while (*s1 && (*s1 == *s2)) {
		s1++;
		s2++;
	}
	return *(const unsigned char*)s1 - *(const unsigned char*)s2;
}

int atoi(const char* str)
{
	int i, val = 0;

	for (i = 0; str[i] != '\0'; ++i)
		val = val * 10 + str[i] - '0';

	return val;
}

/* from: https://github.com/barak/cbp/blob/master/libcs/atoh.c */
uint32_t atoh(char *ap)
{
	uint32_t n = 0;
	char *p = ap;
	int digit, lcase;

	while (*p == ' ' || *p == '\t')
		p++;

	/* detect and discard 0x/0X prefix, should it be present */
	if ((p[0] == '0') && ((p[1] == 'x') || (p[1] == 'X')))
		p += 2;

	while ((digit = (*p >= '0' && *p <= '9')) ||
		(lcase = (*p >= 'a' && *p <= 'f')) ||
		(*p >= 'A' && *p <= 'F')) {
		n *= 16;
		if (digit)
			n += *p++ - '0';
		else if (lcase)
			n += 10 + (*p++ - 'a');
		else
			n += 10 + (*p++ - 'A');
	}

	return n;
}

char *strchr(const char *s, const char c)
{
	if (!s)
		return 0;

	do {
		if (*s == c)
			return (char *)s;
	} while (*s++);

	return 0;
}

char *strstr(char *string, char *substring)
{
	char *a, *b;

	b = substring;
	if (*b == 0)
		return string;

	for (; *string != 0; string++) {
		if (*string != *b)
			continue;

		a = string;

		while (1) {
			if (*b == 0)
				return string;

			if (*a++ != *b++)
				break;

			b = substring;
		}
	}
	return NULL;
}

uint32_t pow(uint8_t base, uint8_t power)
{
	uint32_t n = 1;
	int i;

	for (i = 0; i < power; i++)
		n *= base;

	return n;
}

size_t strlen(char *s)
{
	unsigned int len = 0;

	while (s[len] != '\0')
		len++;

	return len;
}

int printf(const char *fmt, ...)
{
	char buf[128];
	va_list va;
	int len;

	va_start(va, fmt);
	len = mini_vsnprintf(buf, sizeof(buf), fmt, va);
	va_end(va);

	puts(buf);

	return len;
}

void delay_ms(unsigned int ms)
{
	volatile unsigned int i;

	for (i= 0; i < ms*200; i++) { i; }
}

void delay_us(unsigned int us)
{
	volatile unsigned int i;

	for (i= 0; i < us*20; i++) { i; }
}

void dump_mem(uint8_t *dat, int len)
{
	int i;

	for (i = 0; i < len; i++) {
		printf("%02x ", dat[i]);

		if (((i+1) % 16) == 0)
			puts("\n");
	}

	puts("\n");
}
