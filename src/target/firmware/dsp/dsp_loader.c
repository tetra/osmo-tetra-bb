#include <asm.h>
#include <hc16/mem.h>
#include <lib/lib.h>

/* This function is an optimized copy routine from flash to DSP
 * 
 * It requires that the correct flash page is already set up
 * 
 * Arguments: 
 * src_addr - Absolute source address as seen from the CPU - absolute flash address needs to be
 *            converted beforehand!
 * dsp_addr - Base address of the DSP
 * length - Length of the section in 24-bit words
 */
void dsp_l_section(uint32_t src_addr, uint32_t dsp_addr, uint16_t length)
{
	asm("pshm    b,k\n\t"
	    "ldab    %0\n\t"		/* load src page to B */
	    "tbxk\n\t"			/* transfer src page to XK */
	    "ldab    %1\n\t"		/* load dest page to B */
	    "tbyk\n\t"			/* transfer dest page to YK */
	    "ldx     %2\n\t"		/* src addr in X */
	    "ldy     %3\n\t"		/* dest addr in Y */
	    "clre\n\t"
    "loop:  ldd     0,x\n\t"
	    "std     5,y\n\t"		/* load word to DSP addr 5 and 6 */
	    "ldab    2,x\n\r"
	    "stab    7,y\n\r"		/* load last byte to DSP addr 7, complete 24 bit word */
	    "aix     #3\n\r"
	    "adde    #1\n\t"
	    "cpe     %4\n\t"
	    "beq    done\n\t"
	    "bra     loop\n\t"
     "done:  pulm    b,k\n\t"
	    :					/* no output operands */
	    : "memory" (PAGEBYTE(src_addr)),	/* input operand, %0 */
	      "memory" (PAGEBYTE(dsp_addr)),	/* input operand, %1, destination page */
	      "memory" (MEMWORD(src_addr)),	/* input operand, %2 */
	      "memory" (MEMWORD(dsp_addr)),	/* input operand, %3 */
	      "memory" (length)			/* input operand, %4 */
	    : "x", "y", "e", "d");		// fixme, add bk and k to clobber list!
}
