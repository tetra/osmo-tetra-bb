/* DSP56309 image handling as stored in Motorola d700/MTH300 flash
 *
 * This file is part of osmo-tetra-bb.
 *
 * Copyright (C) 2021-2022 by Steve Markgraf <steve@steve-m.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 * osmo-tetra-bb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * osmo-tetra-bb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm.h>
#include <hc16/mem.h>
#include <lib/lib.h>
#include <drivers/flash.h>
#include <dsp/dsp_image.h>

uint32_t dsp_find_version(uint32_t start_addr)
{
	uint8_t tetra_dsp[] = "TETRA DSP";
	uint8_t tbl[512];
	int i;

	do {
		printf("reading flash at %lx\n", start_addr);
		flash_read(start_addr, tbl, sizeof(tbl));

		for (i = 0; i < (sizeof(tbl) - sizeof(tetra_dsp)); i++) {
			if (memcmp(&tbl[i], tetra_dsp, sizeof(tetra_dsp)-1) == 0)
				return tbl[i+12] << 8 | tbl[i+13] ;
		}
	} while (start_addr += (sizeof(tbl) - sizeof(tbl)/4));		/* have some overlap */
	
	return 0;
}

uint32_t dsp_find_bootstrap(void)
{
	/* known bootstrap program addresses: */
	const uint32_t bootstrap_addrs[] = { 0x57b18,	//D700 80
					     0x57b24,	//D700 78a
					     0x5785e,	//D700 77
					     0x5a26c,	// MTH300 new
					     0xcfd46,	// MTH300 old
					   };
	int i;
	const uint8_t bs_len[] = { 0x00, 0x00, 0xB5 };
	uint8_t val[sizeof(bs_len)];

	/* the first word of the bootstrap program is the length word, which is always the same */
	for (i = 0; i < ARRAY_SIZE(bootstrap_addrs); i++) {
		flash_read(bootstrap_addrs[i], val, sizeof(val));

		if (memcmp(bs_len, val, sizeof(bs_len)) == 0)
			return bootstrap_addrs[i];
	}

	return 0;
}

uint32_t dsp_find_image_table(void)
{
	/* The first section of the DSP image is always stored at the same address */
	uint8_t first_entry[] = { 0x00, 0x08, 0x00, 0x04, 0x00 };
	uint8_t tbl[512];
	uint32_t start_addr = 0xdef0;
	int i;

	do {
		flash_read(start_addr, tbl, sizeof(tbl));

		for (i = 0; i < (sizeof(tbl) - sizeof(first_entry)); i++) {
			if (memcmp(&tbl[i], first_entry, sizeof(first_entry)) == 0)
				return start_addr + i;
		}
	} while (start_addr += (sizeof(tbl) - sizeof(tbl)/4));		/* have some overlap */

	return 0;
}

/* This function searches for the start of the data section of the firmware in flash */
uint32_t dsp_find_data_section(void)
{
	/* The begin of the data section in the original firmware has this pattern */
	uint8_t data_start[] = { 0x27, 0xf7, 0x27, 0xf7, 0x27, 0xf7, 0x27, 0xf7, 0x27, 0xf7, 0x27, 0xf7  };
	uint8_t tbl[512];
	uint16_t verify_addr;
	uint32_t start_addr = 0x00d940;
	int i;

	do {
		flash_read(start_addr, tbl, sizeof(tbl));

		for (i = 0; i < (sizeof(tbl) - sizeof(data_start)); i++) {
			if (memcmp(&tbl[i], data_start, sizeof(data_start)) == 0) {
				/* verify address, as the address is also stored at offset -10 */
				start_addr += i;
				flash_read(start_addr-10, &verify_addr, sizeof(verify_addr));
				if (start_addr == verify_addr) {
					return start_addr;
				} else {
					printf("Error, data section start address does not match!\n");
				}
			}
		}
	} while (start_addr += (sizeof(tbl) - sizeof(tbl)/4));		/* have some overlap */

	return 0;
}

struct dsp_entry {
	uint32_t addr;
	uint32_t len_addr;
};

void dsp_read_image_table(struct dsp_section *section)
{
	struct dsp_entry dsp_entries[32];
	uint32_t data_section_start = dsp_find_data_section();
	uint32_t image_table_addr = dsp_find_image_table();
	int i, j = 0;

	printf("Start of data section: %lx\n", data_section_start);
	printf("Image Table: %lx\n", image_table_addr);
	flash_read(image_table_addr, dsp_entries, sizeof(dsp_entries));

	for (i = 0; i < ARRAY_SIZE(dsp_entries); i++) {
		uint32_t addr = dsp_entries[i].addr;
		uint32_t size_addr;
		uint16_t sz;

		if (addr >= 0x80000)
			addr += 0x40000;

		size_addr = data_section_start + WORD0(dsp_entries[i].len_addr);
		flash_read(size_addr, &sz, sizeof(sz));

		/* fill out section list */
		section[j].addr = addr;
		section[j++].len = sz;
		printf("{ 0x%lx, %d },\n", addr, sz);

		/* skip unused entries */
		if (i == 15)
			i += 8;
	}
}
