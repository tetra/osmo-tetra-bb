/* Driver for booting and controlling the DSP56309
 * that implements the TETRA baseband functions
 *
 * This file is part of osmo-tetra-bb.
 *
 * Copyright (C) 2021-2022 by Steve Markgraf <steve@steve-m.de>
 *
 * SPDX-License-Identifier: GPL-2.0+
 *
 * osmo-tetra-bb is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * osmo-tetra-bb is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <asm.h>
#include <hc16/regs.h>
#include <hc16/mem.h>
#include <hc16/spi.h>
#include <hc16/uart.h>
#include <drivers/pmic.h>
#include <drivers/flash.h>
#include <drivers/rf.h>
#include <lib/lib.h>
#include <interrupts.h>
#include <dsp/dsp_loader.h>
#include <dsp/dsp_image.h>
#include <dsp/dsp.h>

#define DSP_BASE_ADDR		0xC0000

#define DSP_RESET (1 << 0)	/* PF0 */

/* At least the MTH800 can listen to this talkgroup, regardless of which TG is configured
 * The MTH300 won't listen unfortunately */
#define TG_ANY		0xffffff

uint32_t talkgroup = 2000;//0x800001;//0x64; 2000;//


typedef enum dsp_rx_state {
	DSP_IDLE,
	RECEIVE_MESSAGE,
} dsp_rx_state;

dsp_rx_state rx_state = DSP_IDLE;

/* DSP host interface register offsets */
#define ICR	0
#define CVR	1

#define ISR	2
#define RXDF	(1 << 0)	/* Receive Data Register Full bit */

#define IVR	3
#define RXH	5
#define RXM	6
#define RXL	7

void dsp_set_modb(void)
{
	uint16_t val;

	/* clear PORTE0 to set DSP MODB low and pulse MODA low for a short time
	 * Attention: this also sets the MUX U424 and disables the routing of the HC16 serial port
	 * to the phone connector! */
	val = readw(DDRE);
	val |= 1;
	writew(val, DDRE);

	val = readw(PORTE0);
	writew(val &= ~1, PORTE0);
}

void dsp_enable_interrupts(bool on)
{
	uint16_t val = readw(PFPAR);

	if (on)
		val |= 0x10;
	else
		val &= ~0x10;

	writew(val, PFPAR);
}

#define dsp_read_and_print	read_and_print

void read_and_print(uint8_t reg, uint8_t expect)
{
	uint8_t val;
	val = readb_p(DSP_BASE_ADDR+reg);

//	if (val != expect)
//		printf("Unexpected value: Reg %02x: %02x, expected %02x\n", reg, val, expect);
}

void dsp_write(uint8_t reg, uint8_t val)
{
	writeb_p(val, DSP_BASE_ADDR + reg);
}

uint8_t dsp_read(uint8_t reg)
{
	return readb_p(DSP_BASE_ADDR + reg);
}

void dsp_write_word(uint8_t *data)
{
//	uint16_t foo = (data[0] << 8) | data[1];
//	writew_p(WORD1(data), DSP_BASE_ADDR+5);	//possible future optimization
	writeb_p(data[0], DSP_BASE_ADDR+5);
	writeb_p(data[1], DSP_BASE_ADDR+6);
	writeb_p(data[2], DSP_BASE_ADDR+7);
//	printf("Wrote %02x%02x%02x ", data[0], data[1], data[2]);
}

uint32_t dsp_read_word(void)
{
	uint32_t data = 0;

	BYTE2(data) = readb_p(DSP_BASE_ADDR+5);
	WORD0(data) = readw_p(DSP_BASE_ADDR+6);

	return data;
}

void dsp_write_u32(uint32_t data)
{
//	uint16_t foo = (data[0] << 8) | data[1];
//	writew_p(foo, DSP_BASE_ADDR+5);	//possible future optimization
	writeb_p(BYTE2(data), DSP_BASE_ADDR+5);
	writeb_p(BYTE1(data), DSP_BASE_ADDR+6);
	writeb_p(BYTE0(data), DSP_BASE_ADDR+7);
//	printf("Wrote %02x%02x%02x ", data[0], data[1], data[2]);
}

void dsp_command(uint32_t cmd)
{
	dsp_enable_interrupts(false);
	dsp_write_u32(cmd);
	dsp_write(ICR, 0x03);
	dsp_write(ICR, 0x01);
	dsp_write(CVR, 0xb2);
	dsp_enable_interrupts(true);
}

void dsp_flush(void)
{
	uint32_t word;

	while (dsp_read(ISR) & RXDF) {
		word = dsp_read_word();
		printf("Flushed DSP word %lx\n", word);
	}
}

#define DSP_MSG_ADD_TO_LENGTH	(1 << 0)	/* And an offset of 1 to length, used for queries */
#define DSP_MSG_NO_INTS		(1 << 1)	/* Don't disable interrupts */
#define DSP_MSG_NO_CVR		(1 << 2)	/* Don't trigger command vector */

void dsp_send_message(uint8_t msg_type, uint32_t *msg, uint8_t msg_len, uint8_t flags)
{
	/* the expected response length is added to the real message length */
	uint8_t len = msg_len + (flags & DSP_MSG_ADD_TO_LENGTH ? 1 : 0);
	uint32_t msg_start = (len << 8) | msg_type;
	//BYTE1(msg_start) = len + resp_len;
	int i;

	if (!(flags & DSP_MSG_NO_INTS))
		dsp_enable_interrupts(false);

//	printf("writing first word: %lx\n", msg_start);
	dsp_write_u32(msg_start);

	dsp_read_and_print(ICR, 0x01);
	dsp_write(ICR, 0x03);		// which bit is orred?
//	dsp_read_and_print(ISR, 0x86);

//	dsp_flush();

	for (i = 0; i < len; i++) {
//		printf("writing dsp word: %lx\n", msg[i]);
		dsp_write_u32(msg[i]);
		dsp_read_and_print(ISR, 0x86);
	}

//	dsp_flush();

	dsp_read_and_print(ICR, 0x03);
	dsp_write(ICR, 0x01);

	/* trigger message command vector on DSP side */
	if (!(flags & DSP_MSG_NO_CVR))
		dsp_write(CVR, 0xb2);

	if (!(flags & DSP_MSG_NO_INTS))
		dsp_enable_interrupts(true);
}

void dsp_read_memory(uint8_t mem, uint32_t addr)
{
	uint32_t msg[] = { mem, addr, 1 };
	dsp_send_message(0x04, msg, ARRAY_SIZE(msg), 0);
}

/* generate beep tone, type 0 or 1 */
void dsp_keypad_beep(uint8_t beeptype)
{
 	uint32_t msg[] = { 0x81281f,
			   beeptype ? 0x070d06 : 0x038906,
			   0x800070, 0x000031, 0x000002, 0x000000,
			   0x0000cb, 0x000000, 0x000084
			   };

//uint16_t a1[] = { 0xBD84, 0x5080 };
//uint16_t a2[] = { 0x8597, 0x0f20 };

	uint16_t a1[] = { 0x8d97, 0x0f20 };
	uint16_t a2[] = { 0xbde4, 0x5082 };

	spi_send(a1, ARRAY_SIZE(a1), 2);
	spi_send(a2, ARRAY_SIZE(a2), 2);

	dsp_command(0x407f4b);		// this sets the volume, highest byte is speaker, 7f max value
					// middle byte earpiece volume?
	/* turn speaker on, turned off again in ISR */
	dsp_write(CVR, 0xb6);


 
	dsp_send_message(0x01, msg, ARRAY_SIZE(msg), 0);
}

void dsp_load_section(const struct dsp_section *section, uint8_t list_len, bool optimized)
{
	int i;
	uint8_t fdata[3];
	uint32_t addr;
	uint8_t cur_section;
	uint16_t words_to_write;

	for (cur_section = 0; cur_section < list_len; cur_section++) {
		words_to_write = section[cur_section].len;
		addr = section[cur_section].addr;

		printf("Loading section %d of %d from %lx with length %d\n", cur_section+1, list_len, addr, words_to_write);

		flash_read(addr, fdata, sizeof(fdata));
		dsp_write_word(&fdata[0]);
		words_to_write--;
		addr += 3;
		dsp_write(ICR, 0x03);

		if (optimized) {
			dsp_l_section(flash_prepare_addr(addr), DSP_BASE_ADDR, words_to_write);
		} else {
			for (i = 0; i < words_to_write; i++) {
				flash_read(addr, fdata, sizeof(fdata));
				dsp_write_word(&fdata[0]);
				addr += 3;
			}
		}

		/* disable TREQ */
		dsp_write(ICR, 0x01);
	}
}

void dsp_init(void)
{
	uint32_t version;
	uint16_t val;
	uint32_t data;
	struct dsp_section dsp_sections[3*8];
	struct dsp_section bootstrap_section;

	dsp_enable_interrupts(false);

	dsp_read_image_table(dsp_sections);

	for (val = 0; val < ARRAY_SIZE(dsp_sections); val++) {
		printf("Entry %d, Addr: %lx, len %d\n", val, dsp_sections[val].addr, dsp_sections[val].len);
	}

	version = dsp_find_version(dsp_sections[1].addr + 0x2a00-512);
	printf("DSP version: %lx\n", version);

	bootstrap_section.addr = dsp_find_bootstrap();
	bootstrap_section.len = 183;

	if (bootstrap_section.addr == 0) {
		printf("Error, DSP bootstrap program not found!\n");
		return;
	}

	// HREQ: IRQx
	// HACK: (goes to A1)
	// (AS: A0 of HC16
	// HA8: A1 of HC16
	// HA9: A2 of HC16


	//mystery_write_f0000:                    ; CODE XREF: sub_46D52↓p
	               //bset    PFPAR, #10h     ; Port F Pin Assignment Reg

	/* DSP MODB seems to be connected to PE0 through some buffers 
	 *     MODC/MODD is hardwired to 1 */



	/* FIXME, right now this generates a glitch on the DSP reset line,
	 * we need to configure the data register first, then configure the pin as ouput */

	/* configure DSP reset as output */
	val = readw(DDRF);
	val |= DSP_RESET;
	writew(val, DDRF);

	/* release DSP from reset (PF0), pull low for at least 600us */
	val = readw(PORTF0);
	writew(val | DSP_RESET, PORTF0);
	delay_ms(50);
	writew(val & ~DSP_RESET, PORTF0);
	delay_ms(20);

	/* this happens before loading the bootstrap program */
	dsp_write(ICR, 0x01);

	delay_ms(10);
	dsp_load_section(&bootstrap_section, 1, false);
	delay_ms(5);
	dsp_load_section(&dsp_sections[0], 8, true);		// load main image
	delay_ms(10);


	/* DSP should now be booted */
	data = dsp_read_word();
	if (data != 0x000042)
		printf("Error, unexpected DSP word after boot: %lx\n", data);

	// here starts the part copied from c syntax

	dsp_command(0x110056);
	dsp_command(0x000041);
	data = dsp_read_word();

	delay_ms(50);

	data = dsp_read_word();
	dsp_write(CVR, 0xb7);

	dsp_command(0x04005d);
	data = dsp_read_word();

	{
	uint32_t msg1[] = { 0xab3b00, 0x027800 };
	dsp_send_message(0x08, msg1, ARRAY_SIZE(msg1), DSP_MSG_NO_INTS);
	}

	delay_ms(5);
	data = dsp_read_word();
	data = dsp_read_word();

	dsp_command(0x00e163);
	data = dsp_read_word();

	dsp_command(0x000e50);
	data = dsp_read_word();

	{
	uint32_t msg[] = { 0x000000, 0xf79200 };
	dsp_send_message(0x07, msg, ARRAY_SIZE(msg), DSP_MSG_NO_INTS);
	}

	data = dsp_read_word();

	dsp_command(0x010143);
	data = dsp_read_word();

	dsp_command(0x000a5a);
	data = dsp_read_word();

	dsp_command(0x000157);
	data = dsp_read_word();

	dsp_command(0x27c046);
	data = dsp_read_word();

	{
	uint32_t msg2[] = { 0x0b0000, 0x000000, 0x140000, 0x140000 };
	dsp_send_message(0x0c, msg2, ARRAY_SIZE(msg2), DSP_MSG_NO_INTS);
	}

	data = dsp_read_word();

	dsp_command(0x000045);
	data = dsp_read_word();

	dsp_command(0x000158);
	data = dsp_read_word();

	dsp_command(0x000147);
	data = dsp_read_word();

	dsp_command(0x000158);
	data = dsp_read_word();

	dsp_command(0x000045);
	data = dsp_read_word();


	{
	uint32_t msg3[] = { 0x000000, 0x000000 };
	dsp_send_message(0x14, msg3, ARRAY_SIZE(msg3), DSP_MSG_NO_INTS);
	}

	delay_ms(5);
	data = dsp_read_word();

	dsp_command(0x000c42);
	data = dsp_read_word();

	dsp_command(0x000941);
	data = dsp_read_word();

	// dsp_set_modb();
	delay_ms(50);

	data = dsp_read_word();

	dsp_write(1, 0xb7);
//	dsp_write(1, 0xb5);

	dsp_command(0x7f024b);
	//dsp_set_modb();
	data = dsp_read_word();

	dsp_command(0x000140);
	data = dsp_read_word();
	data = dsp_read_word();

	delay_ms(50);

	dsp_load_section(&dsp_sections[8], 8, true);		// second image

	data = dsp_read_word();
	if (data != 0x42)
		printf("Unexpected word after DSP boot: %lx\n", data);


	delay_ms(10);
	dsp_read_and_print(2, 0x06);

	delay_ms(200);

	dsp_init_after_2nd_image(420125000);

	delay_ms(200);

	dsp_load_section(&dsp_sections[16], 8, true);		// third (DMO?) image

	data = dsp_read_word();

	delay_ms(10);
	dsp_read_and_print(2, 0x06);

	dsp_enable_interrupts(true);
}


void dsp_init_after_2nd_image(uint32_t freq)
{
	uint16_t a1[] = { 0x8d97, 0x0f20 };
	uint16_t a2[] = { 0xbde4, 0x5082 };

	spi_send(a1, ARRAY_SIZE(a1), PMIC_CS);
	spi_send(a2, ARRAY_SIZE(a2), PMIC_CS);

	dsp_set_params(freq);

//	delay_ms(100);

//	dsp_command(0x7f024b);
//	dsp_command(0x000340);
}

//sets up frequency and talkgroup

// can only be set in 5 MHz steps for now!
//#define TARGET_FREQ	430

uint32_t swap_dsp_word(uint32_t word)
{
	uint8_t tmp = BYTE0(word);
	BYTE0(word) = BYTE2(word);
	BYTE2(word) = tmp;

	return word;
}

uint32_t freqval1;		// -0x400 for 10 MHz higher
uint32_t freqval2;
uint32_t freqval3;
uint32_t freqval4;

void dsp_set_frequency(uint32_t freq)
{
	//uint32_t freq = 420125000;//-6250;
	uint32_t hi_part = (freq-HI_STEP_RES/3) / HI_STEP_RES;
	uint32_t remainder = freq - (hi_part * HI_STEP_RES);
	uint16_t remain_steps = remainder / FREQ_STEP_SIZE;

	int rxdiff = 183 - hi_part; // how far away from 0xb are we
	int txdiff = 182 - hi_part; 

	freqval2 = 0xDEB00 + (remain_steps * 0x20);
	freqval4 = 0x7EE00 + (remain_steps * 0x20);

	freqval1 = 0x180086 | (rxdiff << 8);
	freqval3 = 0x180086 | (txdiff << 8);
}

//423.3:
/*

Writing1 00180886
Writing2 000e2d00
Writing3 00180786
Writing4 00083000

*/

//423.4:

/*
 * 
 * Writing1 00180886
Writing2 000e2f00
Writing3 00180786
Writing4 00083200
*/


void dsp_set_params(uint32_t freq)
{
	{
	uint32_t msg1[] = { 0, 0x028764, swap_dsp_word(freqval1), swap_dsp_word(freqval2), 0x963f00, 0xfdffff, 0xff7ef2,
		           0x028765, swap_dsp_word(freqval3), swap_dsp_word(freqval4), 0x963f00, 0xfdffff,0xfff2f2 };

	uint32_t our_issi = 100;
	uint32_t range = 1;

 	uint32_t msg2[] = { 0xbc4939, talkgroup, our_issi, range };

	while (rx_state != DSP_IDLE) { };
	dsp_enable_interrupts(false);

	dsp_send_message(0x0b, msg1, ARRAY_SIZE(msg1), DSP_MSG_NO_INTS);
	dsp_send_message(0x1a, msg2, ARRAY_SIZE(msg2), DSP_MSG_NO_INTS);
	}
	
	
	printf("Writing1 %lx\n", freqval1);
	printf("Writing2 %lx\n", freqval2);
	printf("Writing3 %lx\n", freqval3);
	printf("Writing4 %lx\n", freqval4);

	dsp_write_u32(0x000a41);
//	dsp_write(ICR, 0x03);
//	dsp_write(ICR, 0x01);
	dsp_write(CVR, 0xb2);

	delay_ms(100);

	dsp_command(0x7f024b);
	dsp_command(0x000340);

	dsp_enable_interrupts(true);
}

uint8_t start_tx_on_next_int = 0;
uint8_t tx_active = 0;

/* start DMO transmission */
void dsp_start_tx(void)
{
 	uint32_t msg[] = { 0x5, 0, 0, 0xbc4939, talkgroup,
			   0, 0, 0, 0, 0, 0x1, 0x8, 0x5, 0, 0, 0, 0, 0, 0, 0, 0x1, 0, 0,
			   0x000518, 0, 0x400000 };


	if (tx_active || start_tx_on_next_int)
		return;

	puts("transmitting!\n");

	dsp_enable_interrupts(false);
	dsp_send_message(0x13, msg, ARRAY_SIZE(msg), DSP_MSG_NO_INTS | DSP_MSG_NO_CVR);
//	tx_active = 1;
	start_tx_on_next_int = 1;

	dsp_enable_interrupts(true);
	pmic_transmit();

	dsp_write(CVR, 0xb7);	/* FIXME: figure out what that does, not needed */
}

bool end_tx_requested = false;

/* end DMO transmission */

void dsp_end_tx(void)
{
	uint32_t msg[] = { 0x5, 0, 0, 0xbc4939, talkgroup,
			   0, 0, 0, 0, 0, 0x3, 0xe, 0x4, 0, 0, 0, 0, 0, 0, 0, 0x1, 0, 0,
			   0x000418, 0, 0x200000 };

	if (!tx_active)
		return;

	if (end_tx_requested)
		return;
//	uint16_t addag[] = { 0x1082, 0x2445, 0x0001 };

//	spi_send(addag, ARRAY_SIZE(addag), ADDAG_CS);

	dsp_send_message(0x13, msg, ARRAY_SIZE(msg), DSP_MSG_NO_CVR);
//	tx_active = 0;
	end_tx_requested = true;
}

void _dsp_end_tx(void)
{

}

void addag_prepare_tx(void)
{
	uint16_t addag[] = { 0x1082, 0x2440, 0x0001 };

	spi_send(addag, ARRAY_SIZE(addag), ADDAG_CS);
	delay_ms(1);
}

#if 0
void proc_95f(void)
{
	uint16_t addag[] = { 0x1082, 0x2440, 0x0001 };
	uint16_t rf1[] = {  0x0e00, 0x2e08, 0x43f2 };


	spi_send(addag, ARRAY_SIZE(addag), ADDAG_CS);
	delay_us(5);
	spi_send(rf1,  ARRAY_SIZE(rf1), RF_CS);
//	delay_us(50);
//	spi_send(rf_before_tx,  sizeof(rf_before_tx), RF_CS);
}
#endif


void proc_95f(void)
{
	int i;
	uint16_t addag[] = { 0x1082, 0x2440, 0x0001 };
	uint16_t rf1[] = {  0x0e00, 0x2e08, 0x43f2 };
	uint16_t rf_before_tx[] = {//mosi	miso
				0x0E04,	//0xFF88
				0xF900,	//0x7D8C
				0x0018,	//0x7E73
				0x1F21,	//0x0140
				0x507C,	//0xFF70
				0x3410,	//0x6C55
				0x4440,	//0x887D
				0x0018,	//0x8C7E
				0x2800,	//0x7301
				0x03bb,	//0x40FF
				0x5100,	//0x706C
				0x031d,	//0x5588
				0x3001,	//0x7D8C
				0x0210, //0x7E73
				0x0F64,	//0x0140
				0x0000,	//0xFF70
			};

	spi_send(addag, ARRAY_SIZE(addag), ADDAG_CS);
	delay_us(5);
	spi_send(rf1,  ARRAY_SIZE(rf1), RF_CS);
//	delay_us(50);
//	spi_send(rf_before_tx,  ARRAY_SIZE(rf_before_tx), RF_CS);
}


//DSP: 00000413 , this message has 4 words that follow
//     00001613: here, 0x16 bytes follow



// Active Incoming call:                                      ISSI: 222 
// 00000004, 0000ff00, 0000ffe8, 00bc4939, 00000064, 00bc4939, 000000de, 00000000, 0000fff0, 0000000d, 00000000, 00000000, 00000005, 00000000, 00000000,
// 00000000, 00000018, 00000018, 00000000, 00000514, 00800004, 00486b30, 



uint32_t dsp_msg[32];


void dsp_msg_dump(uint8_t msg_len)
{
	int i;

	for (i = 0; i < msg_len; i++) {
		printf("%lx, ", dsp_msg[i]);
	}
	printf("\n");
}

void dsp_handle_msg(uint8_t msg_type, uint16_t msg_len)
{
	uint32_t val;
	uint32_t status;
//	uint8_t data[3];

	/* Response to "read DSP memory" command */
	if (msg_type == 0x40 && msg_len == 4) {
		val = dsp_msg[3];
		printf("/* DSP addr %lx: */ 0x%02x, 0x%02x, 0x%02x,\n", dsp_msg[2], BYTE2(val), BYTE1(val), BYTE0(val));
//		data[0] = BYTE2(val);
//		data[1] = BYTE1(val);
//		data[2] = BYTE0(val);
//		uart_putdata(data, 3);
	} else if (msg_type == 0x13) {
		/* Incoming DMO call */
		
		/* This command is required, otherwise DMO calls from an MTH800 are aborted after
		 * a couple of hundred milliseconds. Incoming DMO calls from MTH300 work fine however.. */
		dsp_command(0x00ce63);

		if (dsp_msg[0] == 0x01) {
			dsp_msg_dump(msg_len);
			//if (dsp_msg[2] != 0x04)
			//	printf("DMO call aborted!\n");
		}

		if (dsp_msg[0] != 0x04)
			return;


		status = dsp_msg[9];
		
		if (status == 0x08)
			printf("Incoming DMO call, Talkgroup %ld, ISSI %ld talking\n", dsp_msg[4], dsp_msg[6]);
		
		if (status == 0x0d) {
			printf("DMO call continuing\n");
			return;
		}

		if (status == 0x0e)
			printf("DMO Call ended!\n");

		dsp_msg_dump(msg_len);
		
	} else {
		printf("Received message 0x%x of length %d\n", msg_type, msg_len);
		dsp_msg_dump(msg_len);
	}
}

void proc_05f(void)
{
	int i;
	uint16_t addag[] = { 0x1082, 0x2440, 0x0001 };
	uint16_t rf1[] = {  0x0e00, 0x6800, 0x43f2 };
	uint16_t rf_before_tx[] = {//mosi	miso
				0x0E04,	//0xFF86
				0xF900,	//0xa78e
				0x0018,	//0x7E73
				0x1F21,	//0x0140
				0x507C,	//0xFF71
				0x3410,	//0x6C55
				0x4440,	//0x86A7
				0x0018,	//0x8E7E
				0x283C,	//0x7301
				0x03bb,	//0x40FF	//3abb
				0x5180,	//0x716C
				0xa31d,	//0x5586
				0x3038,	//0xa78e
				0x4010, //0x7E73
				0x0F64,	//0x0140
				0x0000,	//0xFF70
			};
	uint16_t addag2[] = { 0x1067, 0x2440, 0x0003 };

	spi_send(addag, ARRAY_SIZE(addag), ADDAG_CS);
	delay_us(5);
//	spi_send(rf1,  ARRAY_SIZE(rf1), RF_CS);
//	delay_us(50);
	spi_send(rf_before_tx,  ARRAY_SIZE(rf_before_tx), RF_CS);
	dsp_write(1, 0xb7);
//	delay_us(5);
//	spi_send(addag2, ARRAY_SIZE(addag2), ADDAG_CS);
}

void proc_15f(void)
{
	uint16_t addag[] = { 0x1082, 0x2440, 0x0001 };
	uint16_t rf_foo[] = { 0x0e00, 0x0c };
	
	spi_send(addag, ARRAY_SIZE(addag), ADDAG_CS);
//	delay_us(5);
//	spi_send(rf_foo,  TRANSFER_END_ON_BYTE | 2, RF_CS);
}

void dsp_interrupt(void)
{
	uint32_t word;
	uint8_t val;
//	static dsp_rx_state rx_state = DSP_IDLE;
	static uint8_t msg_len = 0;
	static uint8_t msg_index = 0;
	static uint8_t msg_type = 0;

	static int periodic_tx_cnt = 0;

	disable_interrupts();

	if (start_tx_on_next_int) {
		dsp_write(CVR, 0xb2);
		puts("Starting TX!\n");
		start_tx_on_next_int = 0;
		addag_prepare_tx();
		periodic_tx_cnt = 0;
		tx_active = 1;
	}

//	puts("DSP int!\n");

	val = dsp_read(ICR);

	while (1) {
		val = dsp_read(ISR);
		if (!(val & RXDF)) {
			/* no more words to read from DSP, we can safely send the end tx command */
//			if (end_tx_requested && tx_active) {
//				_dsp_end_tx();
//				end_tx_requested = 0;
//			}
			break;
		}

		word = dsp_read_word();

		if (rx_state == RECEIVE_MESSAGE) {
			dsp_msg[msg_index++] = word;
			if (msg_index >= msg_len) {
				rx_state = DSP_IDLE;
				dsp_handle_msg(msg_type, msg_len);
			}
			continue;
		}

		/* receive message type 0x13 */
		if ((BYTE2(word) == 0x00) && ((BYTE0(word) == 0x13) )) { //|| (BYTE0(word) == 0x40)
			rx_state = RECEIVE_MESSAGE;
			msg_len = BYTE1(word);
			msg_index = 0;
			msg_type = BYTE0(word);
			continue;
		}

		if (word == 0x45) {
			/* turn speaker off after beep */
			dsp_write(1, 0xb5);
			puts("Speaker off\n");
			continue;
		}

//		if (word == 0xfe704f) {
//			printf("DMO call started!\n");
		//	continue;
//		}

//		if (word == 0x24e5c5) {
//			printf("DMO call ended!\n");
		//	continue;
//		}

		/* check for receive level */
		if ((BYTE0(word) == 0x4b) && (BYTE2(word) == 0xff)) {
			printf("RX level: %d\n", BYTE1(word));
			continue;
		}




		if (word == 0x00015f) {
	//		proc_15f();
		//	dsp_command(0x000054);
		//	dsp_command(0x28024b);
			
		//	printf("Sending Command 0x000054\n");
			continue;
		}

		if (word == 0x004506) {
		//	dsp_command(0x28024b);
			printf("Sending Command 0x004506\n");
		}

		if (tx_active) {
			if (word == 0x5f) {
				
		//		dsp_write(CVR, 0xb7);
				//proc_05f();
				//dsp_command(0x7f024b);
			}
			
			if (word == 0x95f) {
				periodic_tx_cnt++;
				
				if (periodic_tx_cnt == 50) {
					periodic_tx_cnt = 0;
					dsp_command(0x000054);
				}
				proc_95f();
				continue;
			}
			


			if (word == 0x5f && end_tx_requested) {
			//	if (end_tx_requested && tx_active) {
			//		delay_ms(5);
			//		_dsp_end_tx();
					
					
					end_tx_requested = false;
					tx_active = 0;
					dsp_write(CVR, 0xb2);
					continue;
			//	}
			}
		}


		/* check for idle word */
		if (word == 0x5f)
			continue;
			
		if (word == 0x002447)
			continue;
			
		if (word == 0x002247)
			continue;

		if (word == 0xffff4f)
			continue;

//		printf("DSP: %lx\n", word);
	}

	enable_interrupts();
	//rf_query();
}
